import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'category',
                loadChildren: './category/category.module#MyaccountCategoryModule'
            },
            {
                path: 'sub-category',
                loadChildren: './sub-category/sub-category.module#MyaccountSubCategoryModule'
            },
            {
                path: 'budget-item',
                loadChildren: './budget-item/budget-item.module#MyaccountBudgetItemModule'
            },
            {
                path: 'operation',
                loadChildren: './operation/operation.module#MyaccountOperationModule'
            },
            {
                path: 'budget-item-period',
                loadChildren: './budget-item-period/budget-item-period.module#MyaccountBudgetItemPeriodModule'
            },
            {
                path: 'bank-account',
                loadChildren: './bank-account/bank-account.module#MyaccountBankAccountModule'
            },
            {
                path: 'operation',
                loadChildren: './operation/operation.module#MyaccountOperationModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaccountEntityModule {}
