import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from './operation.service';
import { ISubCategory } from 'app/shared/model/sub-category.model';
import { SubCategoryService } from 'app/entities/sub-category';
import { IUser, UserService } from 'app/core';
import { IBudgetItemPeriod } from 'app/shared/model/budget-item-period.model';
import { BudgetItemPeriodService } from 'app/entities/budget-item-period';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account';

@Component({
    selector: 'jhi-operation-update',
    templateUrl: './operation-update.component.html'
})
export class OperationUpdateComponent implements OnInit {
    operation: IOperation;
    isSaving: boolean;

    subcategories: ISubCategory[];

    users: IUser[];

    budgetitemperiods: IBudgetItemPeriod[];

    bankaccounts: IBankAccount[];
    dateDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected operationService: OperationService,
        protected subCategoryService: SubCategoryService,
        protected userService: UserService,
        protected budgetItemPeriodService: BudgetItemPeriodService,
        protected bankAccountService: BankAccountService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ operation }) => {
            this.operation = operation;
        });
        this.subCategoryService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISubCategory[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISubCategory[]>) => response.body)
            )
            .subscribe((res: ISubCategory[]) => (this.subcategories = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.budgetItemPeriodService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBudgetItemPeriod[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBudgetItemPeriod[]>) => response.body)
            )
            .subscribe((res: IBudgetItemPeriod[]) => (this.budgetitemperiods = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.bankAccountService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBankAccount[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBankAccount[]>) => response.body)
            )
            .subscribe((res: IBankAccount[]) => (this.bankaccounts = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.operation.id !== undefined) {
            this.subscribeToSaveResponse(this.operationService.update(this.operation));
        } else {
            this.subscribeToSaveResponse(this.operationService.create(this.operation));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IOperation>>) {
        result.subscribe((res: HttpResponse<IOperation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSubCategoryById(index: number, item: ISubCategory) {
        return item.id;
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    trackBudgetItemPeriodById(index: number, item: IBudgetItemPeriod) {
        return item.id;
    }

    trackBankAccountById(index: number, item: IBankAccount) {
        return item.id;
    }
}
