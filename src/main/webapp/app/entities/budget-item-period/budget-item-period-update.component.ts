import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IBudgetItemPeriod } from 'app/shared/model/budget-item-period.model';
import { BudgetItemPeriodService } from './budget-item-period.service';
import { IBudgetItem } from 'app/shared/model/budget-item.model';
import { BudgetItemService } from 'app/entities/budget-item';
import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from 'app/entities/operation';

@Component({
    selector: 'jhi-budget-item-period-update',
    templateUrl: './budget-item-period-update.component.html'
})
export class BudgetItemPeriodUpdateComponent implements OnInit {
    budgetItemPeriod: IBudgetItemPeriod;
    isSaving: boolean;

    budgetitems: IBudgetItem[];

    operations: IOperation[];
    dateDp: any;
    monthDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected budgetItemPeriodService: BudgetItemPeriodService,
        protected budgetItemService: BudgetItemService,
        protected operationService: OperationService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ budgetItemPeriod }) => {
            this.budgetItemPeriod = budgetItemPeriod;
        });
        this.budgetItemService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBudgetItem[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBudgetItem[]>) => response.body)
            )
            .subscribe((res: IBudgetItem[]) => (this.budgetitems = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.operationService
            .query({ 'budgetItemId.specified': 'false' })
            .pipe(
                filter((mayBeOk: HttpResponse<IOperation[]>) => mayBeOk.ok),
                map((response: HttpResponse<IOperation[]>) => response.body)
            )
            .subscribe(
                (res: IOperation[]) => {
                    if (!this.budgetItemPeriod.operationId) {
                        this.operations = res;
                    } else {
                        this.operationService
                            .find(this.budgetItemPeriod.operationId)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<IOperation>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<IOperation>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: IOperation) => (this.operations = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.budgetItemPeriod.id !== undefined) {
            this.subscribeToSaveResponse(this.budgetItemPeriodService.update(this.budgetItemPeriod));
        } else {
            this.subscribeToSaveResponse(this.budgetItemPeriodService.create(this.budgetItemPeriod));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBudgetItemPeriod>>) {
        result.subscribe((res: HttpResponse<IBudgetItemPeriod>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBudgetItemById(index: number, item: IBudgetItem) {
        return item.id;
    }

    trackOperationById(index: number, item: IOperation) {
        return item.id;
    }
}
