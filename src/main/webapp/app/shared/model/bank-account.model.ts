export interface IBankAccount {
    id?: number;
    accountName?: string;
    accountBank?: string;
    initialAmount?: number;
    archived?: boolean;
    shortName?: string;
    accountLogin?: string;
    accountId?: number;
}

export class BankAccount implements IBankAccount {
    constructor(
        public id?: number,
        public accountName?: string,
        public accountBank?: string,
        public initialAmount?: number,
        public archived?: boolean,
        public shortName?: string,
        public accountLogin?: string,
        public accountId?: number
    ) {
        this.archived = this.archived || false;
    }
}
