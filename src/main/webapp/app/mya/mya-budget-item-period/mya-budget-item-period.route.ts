import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BudgetItemPeriod } from 'app/shared/model/budget-item-period.model';
import { MyaBudgetItemPeriodService } from './mya-budget-item-period.service';
import { IBudgetItemPeriod } from 'app/shared/model/budget-item-period.model';

@Injectable({ providedIn: 'root' })
export class MyaBudgetItemPeriodResolve implements Resolve<IBudgetItemPeriod> {
    constructor(private service: MyaBudgetItemPeriodService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((budgetItemPeriod: HttpResponse<BudgetItemPeriod>) => budgetItemPeriod.body));
        }
        return of(new BudgetItemPeriod());
    }
}

export const myaBudgetItemPeriodRoute: Routes = [];
