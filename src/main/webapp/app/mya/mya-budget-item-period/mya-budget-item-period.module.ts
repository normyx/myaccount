import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';

import { MyaccountSharedModule } from 'app/shared';
import {
    MyaBudgetItemPeriodUpdateDialogComponent,
    MyaBudgetItemPeriodCreateDialogComponent,
    MyaBudgetItemPeriodDeleteDialogComponent,
    MyaBudgetItemPeriodCellComponent,
    myaBudgetItemPeriodRoute
} from './';

const ENTITY_STATES = [...myaBudgetItemPeriodRoute];

@NgModule({
    imports: [MyaccountSharedModule, InputSwitchModule, InputTextModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        MyaBudgetItemPeriodUpdateDialogComponent,
        MyaBudgetItemPeriodCreateDialogComponent,
        MyaBudgetItemPeriodDeleteDialogComponent,
        MyaBudgetItemPeriodCellComponent
    ],
    entryComponents: [
        MyaBudgetItemPeriodUpdateDialogComponent,
        MyaBudgetItemPeriodCreateDialogComponent,
        MyaBudgetItemPeriodDeleteDialogComponent,
        MyaBudgetItemPeriodCellComponent
    ],
    exports: [MyaBudgetItemPeriodCellComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaccountMyaBudgetItemPeriodModule {}
