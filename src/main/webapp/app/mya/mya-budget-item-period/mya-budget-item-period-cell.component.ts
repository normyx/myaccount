import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatRadioChange, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';

import { Subscription, Observable } from 'rxjs';

import { IBudgetItemPeriod, BudgetItemPeriod } from 'app/shared/model/budget-item-period.model';
import { IBudgetItem } from 'app/shared/model/budget-item.model';
import { IOperation } from 'app/shared/model/operation.model';

import { MyaOperationService } from 'app/mya/mya-operation/mya-operation.service';
import { MyaBudgetItemService } from 'app/mya/mya-budget-item/mya-budget-item.service';
import { MyaBudgetItemPeriodService } from './mya-budget-item-period.service';
import { BudgetItemService } from 'app/entities/budget-item/budget-item.service';

import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';

const moment = _moment;

@Component({
    /* tslint:disable-next-line */
    selector: 'jhi-mya-budget-item-period-cell',
    templateUrl: './mya-budget-item-period-cell.component.html'
})
export class MyaBudgetItemPeriodCellComponent implements OnInit, OnChanges {
    @Input()
    budgetItemPeriod: IBudgetItemPeriod;
    @Input()
    isFirst: boolean;
    @Input()
    isLast: boolean;
    countOperationsClose = 0;
    eventSubscriber: Subscription;
    lastBudgetItemPeriodOfBudgetItem: IBudgetItemPeriod;
    isInFuture: boolean;

    constructor(
        private operationService: MyaOperationService,
        private budgetItemService: MyaBudgetItemService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        public dialog: MatDialog
    ) {}

    ngOnInit() {}

    ngOnChanges() {
        this.loadAll();
    }

    loadAll() {
        if (this.isFirst && !this.budgetItemPeriod.isSmoothed) {
            this.operationService.countCloseToBudgetItemPeriod(this.budgetItemPeriod.id).subscribe(
                (res: HttpResponse<number>) => {
                    this.countOperationsClose = res.body;
                },
                (res: HttpErrorResponse) => {
                    this.onError(res.message);
                }
            );
        }
        const now: Moment = moment();
        this.isInFuture = moment.min(this.budgetItemPeriod.month, now) === now;
    }

    public extend() {
        this.budgetItemService.extend(this.budgetItemPeriod.budgetItemId).subscribe(
            (res: HttpResponse<void>) => {
                this.eventManager.broadcast({ name: 'myaBudgetItemRowModification' + this.budgetItemPeriod.budgetItemId, content: 'OK' });
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openUpdateDialog(): void {
        const dialogRef = this.dialog.open(MyaBudgetItemPeriodUpdateDialogComponent, {
            width: '50%',
            maxWidth: '1000px',
            minWidth: '400px',
            data: { budgetItemPeriod: this.budgetItemPeriod }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }

    openDeleteDialog(): void {
        const dialogRef = this.dialog.open(MyaBudgetItemPeriodDeleteDialogComponent, {
            width: '30%',
            maxWidth: '500px',
            minWidth: '300px',
            data: { budgetItemPeriod: this.budgetItemPeriod }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }

    openCreateDialog(): void {
        const dialogRef = this.dialog.open(MyaBudgetItemPeriodCreateDialogComponent, {
            width: '50%',
            maxWidth: '1000px',
            minWidth: '400px',
            data: { budgetItemPeriod: this.budgetItemPeriod }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
}

export interface BudgetItemPeriodDialogData {
    budgetItemPeriod: IBudgetItemPeriod;
}

@Component({
    selector: 'jhi-mya-budget-item-period-update-dialog',
    templateUrl: 'mya-budget-item-period-update-dialog.component.html'
})
export class MyaBudgetItemPeriodUpdateDialogComponent implements OnInit {
    budgetItemPeriod: IBudgetItemPeriod;
    budgetItem: IBudgetItem;
    isSmoothedForm = new FormControl();
    modifyNextForm = new FormControl();
    isSaving: boolean;
    day: number;
    modifyNext = false;
    operationsClose: IOperation[];
    selectedOperation: IOperation;

    constructor(
        public dialogRef: MatDialogRef<MyaBudgetItemPeriodUpdateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: BudgetItemPeriodDialogData,
        private budgetItemPeriodService: MyaBudgetItemPeriodService,
        private budgetItemService: BudgetItemService,
        private operationService: MyaOperationService,
        private eventManager: JhiEventManager
    ) {
        this.budgetItemPeriod = Object.assign({}, data.budgetItemPeriod);
    }

    ngOnInit() {
        this.isSaving = false;
        this.budgetItemService.find(this.budgetItemPeriod.budgetItemId).subscribe((res: HttpResponse<IBudgetItem>) => {
            this.budgetItem = res.body;
        });
        if (this.budgetItemPeriod.date) {
            this.day = this.budgetItemPeriod.date.date();
        } else {
            this.day = 0;
        }
        if (!this.budgetItemPeriod.isSmoothed) {
            this.operationService.findCloseToBudgetItemPeriod(this.budgetItemPeriod.id).subscribe((res: HttpResponse<IOperation[]>) => {
                this.operationsClose = res.body;
                this.selectedOperation = this.operationsClose.find(el => el.id === this.budgetItemPeriod.operationId);
            });
            this.isSmoothedForm.setValue(false);
        } else {
            this.isSmoothedForm.setValue(true);
        }
        this.modifyNext = this.budgetItemPeriod.isRecurrent;
        this.modifyNextForm.setValue(this.modifyNext);
    }

    private close(): void {
        this.dialogRef.close();
    }

    disableInput() {
        return this.selectedOperation != null || this.budgetItemPeriod.operationId != null;
    }

    onIsSmoothedChange() {
        console.warn(this.isSmoothedForm.value);
        this.budgetItemPeriod.isSmoothed = this.isSmoothedForm.value;
    }

    onModifyNextChange() {
        console.warn(this.isSmoothedForm.value);
        this.modifyNext = this.modifyNextForm.value;
    }

    onOperationUnselect() {
        this.selectedOperation = null;
        this.budgetItemPeriod.operationId = null;
    }

    onOperationSelect($event: MatRadioChange) {
        this.selectedOperation = $event.value;
        this.budgetItemPeriod.date = this.selectedOperation.date;
        this.budgetItemPeriod.amount = this.selectedOperation.amount;
        this.budgetItemPeriod.operationId = this.selectedOperation.id;
        this.day = this.budgetItemPeriod.date.date();
    }

    save() {
        this.isSaving = true;
        let withNext = false;
        if (this.modifyNext && this.budgetItemPeriod.isRecurrent) {
            withNext = true;
        }
        this.subscribeToSaveResponse(this.budgetItemPeriodService.updateWithNext(this.budgetItemPeriod, this.day, withNext));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBudgetItem>>) {
        result.subscribe((res: HttpResponse<IBudgetItem>) => this.onSaveSuccess());
    }

    private onSaveSuccess() {
        this.eventManager.broadcast({ name: 'myaBudgetItemRowModification' + this.budgetItemPeriod.budgetItemId, content: 'OK' });
        this.eventManager.broadcast({ name: 'myaOperationListModification', content: 'OK' });
        this.eventManager.broadcast({ name: 'myaCategoryReportSummary' + this.budgetItem.categoryId, content: 'OK' });
        this.isSaving = false;
        this.close();
    }
}

@Component({
    selector: 'jhi-mya-budget-item-period-delete-dialog',
    templateUrl: 'mya-budget-item-period-delete-dialog.component.html'
})
export class MyaBudgetItemPeriodDeleteDialogComponent {
    budgetItemPeriod: IBudgetItemPeriod;

    constructor(
        private budgetItemPeriodService: MyaBudgetItemPeriodService,
        public dialogRef: MatDialogRef<MyaBudgetItemPeriodDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: BudgetItemPeriodDialogData,
        private eventManager: JhiEventManager
    ) {
        this.budgetItemPeriod = Object.assign({}, data.budgetItemPeriod);
    }

    confirmDelete() {
        this.budgetItemPeriodService.deleteBudgetItemPeriodWithNext(this.budgetItemPeriod.id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'myaBudgetItemRowModification' + this.budgetItemPeriod.budgetItemId,
                content: 'Deleted an budgetItem'
            });
            this.close();
        });
    }

    private close(): void {
        this.dialogRef.close();
    }
}

@Component({
    selector: 'jhi-mya-budget-item-period-create-dialog',
    templateUrl: 'mya-budget-item-period-create-dialog.component.html'
})
export class MyaBudgetItemPeriodCreateDialogComponent implements OnInit {
    newBudgetItemPeriod: IBudgetItemPeriod;
    budgetItemPeriod: IBudgetItemPeriod;
    day: number;

    constructor(
        private budgetItemPeriodService: MyaBudgetItemPeriodService,
        public dialogRef: MatDialogRef<MyaBudgetItemPeriodCreateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: BudgetItemPeriodDialogData,
        private eventManager: JhiEventManager
    ) {
        this.budgetItemPeriod = Object.assign({}, data.budgetItemPeriod);
    }

    ngOnInit() {
        this.day = 1;
        this.newBudgetItemPeriod = new BudgetItemPeriod();
        this.newBudgetItemPeriod.amount = 0;
        this.newBudgetItemPeriod.isSmoothed = false;
        this.newBudgetItemPeriod.isRecurrent = false;
    }

    private close(): void {
        this.dialogRef.close();
    }

    save() {
        this.newBudgetItemPeriod.month = this.budgetItemPeriod.month;
        if (!this.newBudgetItemPeriod.isSmoothed) {
            this.newBudgetItemPeriod.date = moment();
            this.newBudgetItemPeriod.date.year(this.newBudgetItemPeriod.month.year());
            this.newBudgetItemPeriod.date.month(this.newBudgetItemPeriod.month.month());
            this.newBudgetItemPeriod.date.date(this.day);
        }
        this.newBudgetItemPeriod.budgetItemId = this.budgetItemPeriod.budgetItemId;
        this.newBudgetItemPeriod.month = this.budgetItemPeriod.month;
        this.subscribeToSaveResponse(this.budgetItemPeriodService.create(this.newBudgetItemPeriod));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBudgetItem>>) {
        result.subscribe((res: HttpResponse<IBudgetItem>) => this.onSaveSuccess());
    }

    private onSaveSuccess() {
        this.eventManager.broadcast({ name: 'myaBudgetItemRowModification' + this.budgetItemPeriod.budgetItemId, content: 'OK' });
        this.close();
    }
}
