import { Component, OnInit, Input, OnChanges, OnDestroy, Inject } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { IBudgetItem, BudgetItem } from 'app/shared/model/budget-item.model';
import { IBudgetItemPeriod } from 'app/shared/model/budget-item-period.model';
import { MyaBudgetItemPeriodService } from './../mya-budget-item-period/mya-budget-item-period.service';
import { MyaBudgetItemService } from './mya-budget-item.service';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as Moment from 'moment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category';
import { Observable } from 'rxjs';

@Component({
    /* tslint:disable-next-line */
    selector: '[jhi-mya-budget-item-row]',
    templateUrl: './mya-budget-item-row.component.html'
})
export class MyaBudgetItemRowComponent implements OnInit, OnChanges, OnDestroy {
    @Input()
    budgetItem: IBudgetItem;
    @Input()
    isFirstInList: boolean;
    @Input()
    isLastInList: boolean;
    @Input()
    monthsToDisplay: Date[];
    budgetItemPeriods: IBudgetItemPeriod[][];
    eventSubscriber: Subscription;
    lastBudgetItemPeriodOfBudgetItem: IBudgetItemPeriod;

    constructor(
        private budgetItemService: MyaBudgetItemService,
        private budgetItemPeriodService: MyaBudgetItemPeriodService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        this.loadAll();
    }

    ngOnChanges() {
        this.loadAll();
        this.registerChangeInBudgetItemRow();
    }

    loadAll() {
        this.budgetItemService.find(this.budgetItem.id).subscribe(
            (res: HttpResponse<IBudgetItem>) => {
                this.budgetItem = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        const lastMonth: Date = this.monthsToDisplay[this.monthsToDisplay.length - 1];
        const firstMonth: Date = this.monthsToDisplay[0];
        const criteria = {
            'budgetItemId.equals': this.budgetItem.id,
            'month.greaterOrEqualThan': Moment(firstMonth).format('YYYY-MM-DD'),
            'month.lessOrEqualThan': Moment(lastMonth).format('YYYY-MM-DD'),
            sort: ['isRecurrent,desc']
        };
        let budgetItemPeriodQueryResult: IBudgetItemPeriod[];
        this.budgetItemPeriodService.query(criteria).subscribe(
            (res: HttpResponse<IBudgetItemPeriod[]>) => {
                budgetItemPeriodQueryResult = res.body;
                this.budgetItemPeriods = new Array(this.monthsToDisplay.length);
                let i: number;
                if (budgetItemPeriodQueryResult) {
                    // if result is defined
                    for (i = 0; i < this.monthsToDisplay.length; i++) {
                        const month: Date = this.monthsToDisplay[i];
                        // find corresponding budgetItemPeriod
                        const correspondingBudgetItemPeriod: IBudgetItemPeriod[] = budgetItemPeriodQueryResult.filter(function(el) {
                            return el.month.month() === month.getMonth() && el.month.year() === month.getFullYear();
                        });
                        this.budgetItemPeriods[i] = correspondingBudgetItemPeriod;
                        // console.log(correspondingBudgetItemPeriod);
                    }
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.budgetItemService.lastBudgetItem(this.budgetItem.id).subscribe(
            (res: HttpResponse<IBudgetItemPeriod>) => {
                this.lastBudgetItemPeriodOfBudgetItem = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        // console.log(this.budgetItemPeriods);
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    registerChangeInBudgetItemRow() {
        this.eventSubscriber = this.eventManager.subscribe('myaBudgetItemRowModification' + this.budgetItem.id, response => this.loadAll());
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    public isLast(bip: IBudgetItemPeriod): boolean {
        return this.lastBudgetItemPeriodOfBudgetItem && this.lastBudgetItemPeriodOfBudgetItem.id === bip.id && bip.isRecurrent;
    }

    public up() {
        this.budgetItemService.up(this.budgetItem.id).subscribe(
            (res: HttpResponse<void>) => {
                this.eventManager.broadcast({ name: 'myaBudgetItemListModification', content: 'OK' });
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    public down() {
        this.budgetItemService.down(this.budgetItem.id).subscribe(
            (res: HttpResponse<void>) => {
                this.eventManager.broadcast({ name: 'myaBudgetItemListModification', content: 'OK' });
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    openDeleteDialog(): void {
        const dialogRef = this.dialog.open(MyaBudgetItemDeleteDialogComponent, {
            width: '350px',
            data: { budgetItem: this.budgetItem }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }

    openUpdateDialog(): void {
        const dialogRef = this.dialog.open(MyaBudgetItemUpdateDialogComponent, {
            width: '50%',
            maxWidth: '1000px',
            minWidth: '400px',
            data: { budgetItem: this.budgetItem }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
}

export interface DeleteDialogData {
    budgetItem: IBudgetItem;
}

@Component({
    selector: 'jhi-mya-budget-item-delete-dialog',
    templateUrl: 'mya-budget-item-delete-dialog.component.html'
})
export class MyaBudgetItemDeleteDialogComponent {
    budgetItem: IBudgetItem;

    constructor(
        public dialogRef: MatDialogRef<MyaBudgetItemDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DeleteDialogData,
        private budgetItemService: MyaBudgetItemService,
        private eventManager: JhiEventManager
    ) {
        this.budgetItem = data.budgetItem;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
    confirmDelete() {
        this.budgetItemService.deleteWithPeriods(this.budgetItem.id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'myaBudgetItemListModification',
                content: 'Deleted an budgetItem'
            });
            this.dialogRef.close();
        });
    }
}

export interface UpdateDialogData {
    budgetItem: IBudgetItem;
}

@Component({
    selector: 'jhi-mya-budget-item-update-dialog',
    templateUrl: 'mya-budget-item-update-dialog.component.html'
})
export class MyaBudgetItemUpdateDialogComponent implements OnInit {
    budgetItem: IBudgetItem;
    categories: ICategory[];
    isSaving: boolean;

    constructor(
        public dialogRef: MatDialogRef<MyaBudgetItemUpdateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: UpdateDialogData,
        private budgetItemService: MyaBudgetItemService,
        private categoryService: CategoryService,
        private eventManager: JhiEventManager
    ) {
        this.budgetItem = Object.assign({}, data.budgetItem);
    }

    ngOnInit() {
        this.isSaving = false;
        this.categoryService.query().subscribe((res: HttpResponse<ICategory[]>) => {
            this.categories = res.body;
        });
    }

    private close(): void {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.close();
    }

    save() {
        console.warn(this.budgetItem);
        this.isSaving = true;
        this.subscribeToSaveResponse(this.budgetItemService.update(this.budgetItem));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBudgetItem>>) {
        result.subscribe((res: HttpResponse<IBudgetItem>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.eventManager.broadcast({ name: 'myaBudgetItemRowModification' + this.budgetItem.id, content: 'OK' });
        this.isSaving = false;
        this.close();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
