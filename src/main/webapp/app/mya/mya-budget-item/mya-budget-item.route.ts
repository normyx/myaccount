import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BudgetItem } from 'app/shared/model/budget-item.model';
import { MyaBudgetItemService } from './mya-budget-item.service';
import { MyaBudgetItemComponent } from './mya-budget-item.component';
import { IBudgetItem } from 'app/shared/model/budget-item.model';

@Injectable({ providedIn: 'root' })
export class MyaBudgetItemResolve implements Resolve<IBudgetItem> {
    constructor(private service: MyaBudgetItemService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((budgetItem: HttpResponse<BudgetItem>) => budgetItem.body));
        }
        return of(new BudgetItem());
    }
}

export const myaBudgetItemRoute: Routes = [
    {
        path: 'mya-budget-item',
        component: MyaBudgetItemComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Gestion du Budget'
        },
        canActivate: [UserRouteAccessService]
    }
];
