import { Component, OnInit, Inject } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Observable } from 'rxjs';
import { IBudgetItem, BudgetItem } from 'app/shared/model/budget-item.model';
import { ICategory, Category } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { MyaBudgetItemService } from './mya-budget-item.service';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';

const moment = _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
    parse: {
        dateInput: 'MMMM YYYY'
    },
    display: {
        dateInput: 'MMMM YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};

@Component({
    selector: 'jhi-mya-budget-item',
    templateUrl: './mya-budget-item.component.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
    ]
})
export class MyaBudgetItemComponent implements OnInit {
    currentAccount: any;
    // selectedMonth: Date;
    filterCategories: ICategory[];
    filterSelectedCategory: ICategory;
    filterContains: string;
    dateFormControl = new FormControl({ value: moment(), disabled: true });
    selectedMonth: Date;

    constructor(
        private categoryService: CategoryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        public dialog: MatDialog
    ) {
        // get the current time
        const current: Moment = moment();
        current.date(1);
        // set the selected date to this month
        this.dateFormControl.setValue(current);
        this.selectedMonth = new Date(this.dateFormControl.value.year(), this.dateFormControl.value.month(), 1);
    }

    chosenMonthHandler(normlizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
        const ctrlValue = this.dateFormControl.value;
        ctrlValue.year(normlizedMonth.year());
        ctrlValue.month(normlizedMonth.month());
        ctrlValue.date(1);
        this.dateFormControl.setValue(ctrlValue);
        this.selectedMonth = new Date(ctrlValue.year(), ctrlValue.month(), 1);
        datepicker.close();
    }

    loadAll() {}

    ngOnInit() {
        this.categoryService.query().subscribe(
            (res: HttpResponse<ICategory[]>) => {
                this.filterCategories = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.loadAll();
    }

    trackId(index: number, item: IBudgetItem) {
        return item.id;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    openCreateDialog(): void {
        const dialogRef = this.dialog.open(MyaBudgetItemCreateDialogComponent, {
            width: '50%',
            maxWidth: '1000px',
            minWidth: '400px'
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
}

@Component({
    selector: 'jhi-mya-budget-item-create-dialog',
    templateUrl: 'mya-budget-item-create-dialog.component.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
    ]
})
export class MyaBudgetItemCreateDialogComponent implements OnInit {
    budgetItem: IBudgetItem;
    month: Date;
    smoothed: boolean;
    amount: number;
    dayOfMonth: number;
    categories: Category[];
    selectedCategory: Category;
    dateFormControl = new FormControl(moment());

    constructor(
        public dialogRef: MatDialogRef<MyaBudgetItemCreateDialogComponent>,
        private budgetItemService: MyaBudgetItemService,
        private categoryService: CategoryService,
        private eventManager: JhiEventManager
    ) {
        // get the current time
        const current: Moment = moment();
        current.date(1);
        // set the selected date to this month
        this.dateFormControl.setValue(current);
        this.month = new Date(this.dateFormControl.value.year(), this.dateFormControl.value.month(), 1);
        this.smoothed = false;
        this.dayOfMonth = 1;
        this.budgetItem = new BudgetItem();
    }

    chosenYearHandler(normalizedYear: Moment) {
        const ctrlValue = this.dateFormControl.value;
        ctrlValue.year(normalizedYear.year());
        this.dateFormControl.setValue(ctrlValue);
    }

    chosenMonthHandler(normlizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
        const ctrlValue = this.dateFormControl.value;
        ctrlValue.month(normlizedMonth.month());
        ctrlValue.date(1);
        this.dateFormControl.setValue(ctrlValue);
        this.month = new Date(ctrlValue.year(), ctrlValue.month(), 1);
        datepicker.close();
    }
    ngOnInit() {
        this.categoryService.query().subscribe((res: HttpResponse<ICategory[]>) => {
            this.categories = res.body;
        });
    }

    private close(): void {
        this.dialogRef.close();
    }

    save() {
        this.budgetItem.categoryId = this.selectedCategory.id;
        this.subscribeToSaveResponse(
            this.budgetItemService.createWithBudgetItemPeriods(this.budgetItem, this.smoothed, this.month, this.amount, this.dayOfMonth)
        );
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBudgetItem>>) {
        result.subscribe((res: HttpResponse<IBudgetItem>) => this.onSaveSuccess());
    }

    private onSaveSuccess() {
        this.eventManager.broadcast({
            name: 'myaBudgetItemListModification',
            content: 'Deleted an budgetItem'
        });
        this.close();
    }
}
