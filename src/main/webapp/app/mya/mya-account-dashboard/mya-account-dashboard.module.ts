import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MyaccountSharedModule } from 'app/shared';
import { MyaccountMyaDashboardUIComponentModule } from 'app/mya/mya-dashboard-ui-components/mya-dashboard-ui-components.module';
import { MyaccountMyaBudgetItemModule } from 'app/mya/mya-budget-item/mya-budget-item.module';
import { MyaccountMyaOperationModule } from 'app/mya/mya-operation/mya-operation.module';
import { MyaAccountDashboardComponent, MyaCategoryDashboardComponent, myaAccountDashboardRoute } from './';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule, CalendarModule, SliderModule, AccordionModule, CardModule } from 'primeng/primeng';
const DASHBOARD_STATES = [...myaAccountDashboardRoute];

@NgModule({
    imports: [
        MyaccountSharedModule,
        BrowserAnimationsModule,
        ButtonModule,
        CalendarModule,
        SliderModule,
        AccordionModule,
        CardModule,
        MyaccountMyaDashboardUIComponentModule,
        MyaccountMyaBudgetItemModule,
        MyaccountMyaOperationModule,
        RouterModule.forChild(DASHBOARD_STATES)
    ],
    declarations: [MyaAccountDashboardComponent, MyaCategoryDashboardComponent],
    entryComponents: [MyaCategoryDashboardComponent],
    exports: [MyaCategoryDashboardComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaccountMyaAccountDashboardModule {}
