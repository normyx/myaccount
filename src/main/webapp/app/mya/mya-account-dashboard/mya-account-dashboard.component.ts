import { Component, OnInit } from '@angular/core';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';
import { FormControl } from '@angular/forms';
import { MatDatepicker, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

const moment = _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS_1 = {
    parse: {
        dateInput: 'MMMM YYYY'
    },
    display: {
        dateInput: 'MMMM YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};

@Component({
    selector: 'jhi-mya-account-dashboard',
    templateUrl: './mya-account-dashboard.component.html',
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_1 }
    ]
})
export class MyaAccountDashboardComponent implements OnInit {
    currentMonth: Date;
    balanceDateFrom: Date;
    balanceDateTo: Date;
    numberOfMonths: number;
    currentMonthMinusNumberOfMonth: Date;
    selectedMonth: Date;
    categoryIds: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    selectedDateFormControl = new FormControl({ value: moment(), disabled: true });

    constructor() {
        const current: Date = new Date(Date.now());
        this.currentMonth = new Date(current.getFullYear(), current.getMonth(), 1);
        this.selectedMonth = this.currentMonth;
        this.numberOfMonths = 12;
        this.balanceDateFrom = new Date(current.getFullYear(), current.getMonth() - this.numberOfMonths, current.getDate());
        this.balanceDateTo = new Date(current.getFullYear(), current.getMonth() + this.numberOfMonths, current.getDate());
        this.currentMonthMinusNumberOfMonth = new Date(
            this.currentMonth.getFullYear(),
            this.currentMonth.getMonth() - this.numberOfMonths,
            1
        );
        this.selectedDateFormControl.setValue(moment(this.currentMonth));
    }

    ngOnInit() {}

    chosenMonthHandler(normlizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
        const ctrlValue = this.selectedDateFormControl.value;
        ctrlValue.year(normlizedMonth.year());
        ctrlValue.month(normlizedMonth.month());
        ctrlValue.date(1);
        this.selectedDateFormControl.setValue(ctrlValue);
        this.setMonths(new Date(ctrlValue.year(), ctrlValue.month(), 1));
        datepicker.close();
    }

    private setMonths(monthTo: Date) {
        this.selectedMonth = monthTo;
        this.balanceDateFrom = new Date(this.selectedMonth.getFullYear(), this.selectedMonth.getMonth() - 12, 1);
    }
}
