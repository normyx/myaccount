import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserRouteAccessService } from 'app/core';
import { MyaAccountDashboardComponent } from './mya-account-dashboard.component';
import { MyaCategoryDashboardComponent } from './mya-category-dashboard.component';
import { ICategory, Category } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class CategoryResolve implements Resolve<ICategory> {
    constructor(private service: CategoryService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Category> {
        const id = route.params['catid'] ? route.params['catid'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Category>) => response.ok),
                map((category: HttpResponse<Category>) => category.body)
            );
        }
        return of(new Category());
    }
}

export const myaAccountDashboardRoute: Routes = [
    {
        path: 'accountdashboard',
        component: MyaAccountDashboardComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tableau de bord global'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'mya-category-dashboard/:catid',
        component: MyaCategoryDashboardComponent,
        resolve: {
            category: CategoryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tableau de bord par Catégorie'
        },
        canActivate: [UserRouteAccessService]
    }
];
