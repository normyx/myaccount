import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MyaccountMyaBudgetItemModule } from './mya-budget-item/mya-budget-item.module';
import { MyaccountMyaBudgetItemPeriodModule } from './mya-budget-item-period/mya-budget-item-period.module';
import { MyaccountMyaOperationModule } from './mya-operation/mya-operation.module';
import { MyaCategoryIconModule } from './mya-category-icon/mya-category-icon.module';
import { MyaMaterialFileUploadModule } from './mya-material-file-upload/mya-material-file-upload.module';

@NgModule({
    // prettier-ignore
    imports: [
        MyaCategoryIconModule,
        MyaccountMyaBudgetItemModule,
        MyaccountMyaBudgetItemPeriodModule,
        MyaccountMyaOperationModule,
        MyaMaterialFileUploadModule
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaccountMyaModule {}
