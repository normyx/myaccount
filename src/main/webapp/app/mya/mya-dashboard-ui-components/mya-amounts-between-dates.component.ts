import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { MyaDashboardUIComponentsService } from './mya-dashboard-ui-components.service';
// import { IAccountCategoryMonthReport } from './account-category-month-report.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ChartModule } from 'primeng/chart';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as moment from 'moment';
import { ICategory } from 'app/shared/model/category.model';

@Component({
    selector: 'jhi-mya-amounts-between-dates',
    templateUrl: './mya-amounts-between-dates.component.html'
})
export class MyaAmountsBetweenDatesComponent implements OnInit, OnChanges {
    @Input() dateFrom: Date;
    @Input() dateTo: Date;
    @Input() height = '30vh';
    data: any;
    options: any;
    max: number;
    min: number;

    constructor(private dashboardUIComponentsService: MyaDashboardUIComponentsService, private jhiAlertService: JhiAlertService) {}

    private feedReportData(res: HttpResponse<any>) {
        this.data = {
            labels: res.body.dates,
            datasets: [
                {
                    label: 'Operations',
                    data: res.body.amounts,
                    borderColor: '#0099ff',
                    backgroundColor: '#0099ff',
                    fill: false,
                    pointRadius: 0
                },
                {
                    label: 'Projections',
                    data: res.body.predictiveAmounts,
                    borderColor: '#ff0000',
                    backgroundColor: '#ff0000',
                    fill: false,
                    pointRadius: 0
                }
            ]
        };
        this.options = {
            title: {
                display: false,
                text: 'Evolutions du solde',
                fontSize: 12
            },
            legend: {
                display: false,
                position: 'bottom'
            },
            scales: {
                yAxes: [
                    {
                        scaleLabel: {
                            display: false,
                            labelString: 'Montants'
                        },
                        ticks: {
                            callback(value, index, values) {
                                return value + ' €';
                            }
                        }
                    }
                ],
                xAxes: [
                    {
                        display: true,
                        type: 'time',
                        time: {
                            unit: 'month',
                            stepSize: 1,
                            displayFormats: {
                                month: 'MMM YYYY'
                            }
                        }
                    }
                ]
            },
            tooltips: {
                position: 'average',
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: (tooltipItem, data) => {
                        let label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if (label) {
                            label += ' : ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        label += ' €';
                        return label;
                    }
                }
            }
        };
        if (this.min) {
            this.options.scales.yAxes[0].ticks.min = this.min;
        }
        if (this.max) {
            this.options.scales.yAxes[0].ticks.max = this.max;
        }
    }

    loadAll() {
        if (this.dateFrom && this.dateTo) {
            this.dashboardUIComponentsService
                .getAmountsBetweenDates(moment(this.dateFrom), moment(this.dateTo))
                .subscribe((res: HttpResponse<any>) => this.feedReportData(res), (res: HttpErrorResponse) => this.onError(res.message));
        }
    }

    ngOnInit() {}

    ngOnChanges() {
        this.loadAll();
    }
    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
