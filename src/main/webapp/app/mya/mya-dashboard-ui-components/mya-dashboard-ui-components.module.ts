import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MyaccountSharedModule } from 'app/shared';
import { ChartModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule, CalendarModule, SliderModule, AccordionModule } from 'primeng/primeng';

import {
    MyaAmountGlobalPerDayInMonthReportComponent,
    MyaAmountCategoryPerMonthReportComponent,
    MyaCategoryReportSummaryComponent,
    MyaAmountWeatherStatusComponent,
    MyaAmountEvolutionWithAverageComponent,
    MyaAmountEvolutionWithSmoothedAndMarkedComponent,
    MyaAmountCategorySplitComponent,
    MyaCategoryReportDetailComponent,
    MyaAmountsBetweenDatesComponent
} from './';

@NgModule({
    imports: [
        MyaccountSharedModule,
        ChartModule,
        BrowserAnimationsModule,
        ButtonModule,
        CalendarModule,
        SliderModule,
        AccordionModule,
        RouterModule
    ],
    declarations: [
        MyaAmountGlobalPerDayInMonthReportComponent,
        MyaAmountCategoryPerMonthReportComponent,
        MyaCategoryReportSummaryComponent,
        MyaAmountWeatherStatusComponent,
        MyaAmountEvolutionWithAverageComponent,
        MyaAmountEvolutionWithSmoothedAndMarkedComponent,
        MyaAmountCategorySplitComponent,
        MyaCategoryReportDetailComponent,
        MyaAmountsBetweenDatesComponent
    ],
    exports: [
        MyaAmountGlobalPerDayInMonthReportComponent,
        MyaAmountCategoryPerMonthReportComponent,
        MyaCategoryReportSummaryComponent,
        MyaAmountWeatherStatusComponent,
        MyaAmountEvolutionWithAverageComponent,
        MyaAmountEvolutionWithSmoothedAndMarkedComponent,
        MyaAmountCategorySplitComponent,
        MyaCategoryReportDetailComponent,
        MyaAmountsBetweenDatesComponent
    ],
    entryComponents: [
        MyaAmountGlobalPerDayInMonthReportComponent,
        MyaAmountCategoryPerMonthReportComponent,
        MyaCategoryReportSummaryComponent,
        MyaAmountWeatherStatusComponent,
        MyaAmountEvolutionWithAverageComponent,
        MyaAmountEvolutionWithSmoothedAndMarkedComponent,
        MyaAmountCategorySplitComponent,
        MyaCategoryReportDetailComponent,
        MyaAmountsBetweenDatesComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaccountMyaDashboardUIComponentModule {}
