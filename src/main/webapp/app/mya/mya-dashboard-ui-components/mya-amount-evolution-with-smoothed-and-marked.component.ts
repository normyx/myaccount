import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { MyaDashboardUIComponentsService } from './mya-dashboard-ui-components.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as moment from 'moment';
import { ICategory } from 'app/shared/model/category.model';

@Component({
    selector: 'jhi-mya-amount-evolution-with-smoothed-and-marked',
    templateUrl: './mya-amount-evolution-with-smoothed-and-marked.component.html'
})
export class MyaAmountEvolutionWithSmoothedAndMarkedComponent implements OnInit, OnChanges {
    @Input() category: ICategory;
    @Input() monthTo: Date;
    @Input() monthFrom: Date;
    @Input() height: string;
    data: any;
    options: any;
    eventSubscriber: Subscription;

    constructor(
        private dashboardUIComponentsService: MyaDashboardUIComponentsService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService
    ) {}

    loadAll() {
        if (this.category && this.monthTo && this.monthFrom) {
            this.dashboardUIComponentsService
                .getAmountCategoryPerMonthWithMarked(this.category.id, moment(this.monthFrom), moment(this.monthTo))
                .subscribe(
                    (res: HttpResponse<any>) => {
                        this.data = {
                            labels: res.body.months,
                            datasets: [
                                {
                                    label: 'Montant',
                                    data: res.body.operationAmounts,
                                    borderColor: '#49ab81',
                                    backgroundColor: '#49ab81',
                                    fill: false,
                                    pointRadius: 0
                                },
                                {
                                    label: 'Budget non lissé pointé',
                                    data: res.body.budgetUnSmoothedMarkedAmounts,
                                    borderColor: '#3b5998',
                                    backgroundColor: '#3b5998',
                                    fill: true,
                                    pointRadius: 0
                                },
                                {
                                    label: 'Montant non lissé non pointé',
                                    data: res.body.budgetUnSmoothedUnMarkedAmounts,
                                    borderColor: '#8b9dc3',
                                    backgroundColor: '#8b9dc3',
                                    fill: true,
                                    borderWidth: 2,
                                    pointRadius: 0
                                },
                                {
                                    label: 'Budget Lissé',
                                    data: res.body.budgetSmoothedAmounts,
                                    borderColor: '#dfe3ee',
                                    backgroundColor: '#dfe3ee',
                                    fill: true,
                                    borderWidth: 2,
                                    pointRadius: 0
                                }
                            ]
                        };
                        this.options = {
                            responsive: true,
                            /* title: {
                                display: true,
                                text: 'Evolutions par pointage',
                                fontSize: 12
                            }, */
                            legend: {
                                display: false,
                                position: 'bottom'
                            },
                            scales: {
                                yAxes: [
                                    {
                                        scaleLabel: {
                                            display: false,
                                            labelString: 'Montants'
                                        },
                                        ticks: {
                                            suggestedMax: 0,
                                            callback(value, index, values) {
                                                return value + ' €';
                                            }
                                        }
                                    }
                                ],
                                xAxes: [
                                    {
                                        display: false
                                    }
                                ]
                            },
                            tooltips: {
                                position: 'average',
                                mode: 'index',
                                intersect: false,
                                callbacks: {
                                    label: (tooltipItem, data) => {
                                        let label = data.datasets[tooltipItem.datasetIndex].label || '';
                                        if (label) {
                                            label += ' : ';
                                        }
                                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                                        label += ' €';
                                        return label;
                                    }
                                }
                            }
                        };
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    ngOnInit() {}

    ngOnChanges() {
        this.loadAll();
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
