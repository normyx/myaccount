import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';

@Component({
    selector: 'jhi-mya-category-report-summary',
    templateUrl: './mya-caterory-report-summary.component.html'
})
export class MyaCategoryReportSummaryComponent implements OnInit, OnChanges, OnDestroy {
    @Input()
    categoryId: number;
    @Input()
    monthTo: Date;
    @Input()
    monthFrom: Date;
    category: Observable<ICategory>;
    eventSubscriber: Subscription;

    constructor(
        private categoryService: CategoryService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService
    ) {}

    loadAll() {
        if (this.categoryId) {
            this.category = this.categoryService.find(this.categoryId).pipe(
                map(
                    (res: HttpResponse<ICategory>) => {
                        return res.body;
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                )
            );
        }
    }

    ngOnInit() {
        this.registerChangeInBudgetItems();
    }

    ngOnChanges() {
        this.loadAll();
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }
    registerChangeInBudgetItems() {
        this.eventSubscriber = this.eventManager.subscribe('myaCategoryReportSummary' + this.categoryId, response => this.loadAll());
    }
    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
