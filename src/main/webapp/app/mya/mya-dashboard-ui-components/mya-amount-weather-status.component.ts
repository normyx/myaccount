import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { MyaDashboardUIComponentsService } from './mya-dashboard-ui-components.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-mya-amount-weather-status',
    templateUrl: './mya-amount-weather-status.component.html'
})
export class MyaAmountWeatherStatusComponent implements OnInit, OnChanges, OnDestroy {
    @Input() monthFrom: Date;
    @Input() monthTo: Date;
    @Input() categoryId: number;
    @Input() isSummary = false;
    budgetADate: number;
    operationADate: number;
    data: any;
    options: any;
    eventSubscriber: Subscription;

    constructor(private dashboardUIComponentsService: MyaDashboardUIComponentsService, private jhiAlertService: JhiAlertService) {}

    loadAll() {
        this.dashboardUIComponentsService
            .getAmountCategoryPerMonthWithMarked(this.categoryId, moment(this.monthFrom), moment(this.monthTo))
            .subscribe(
                (res: HttpResponse<any>) => {
                    this.budgetADate = res.body.budgetAtDateAmounts[res.body.budgetAtDateAmounts.length - 1];
                    this.operationADate = res.body.operationAmounts[res.body.operationAmounts.length - 1];
                    if (!this.isSummary) {
                        this.data = {
                            labels: [''],
                            datasets: [
                                {
                                    label: 'Budget',
                                    data: [this.budgetADate],
                                    borderColor: '#000000',
                                    backgroundColor: '#ffffff00',
                                    fill: false,
                                    borderWidth: 4
                                },
                                {
                                    label: 'Operations',
                                    data: [this.operationADate],
                                    borderColor: '#49ab81',
                                    backgroundColor: '#49ab81',
                                    borderWidth: 0,
                                    fill: false
                                }
                            ]
                        };
                        this.options = {
                            title: {
                                display: true,
                                text: 'Consommation du budget',
                                fontSize: 12
                            },
                            legend: {
                                position: 'bottom'
                            },

                            scales: {
                                yAxes: [
                                    {
                                        stacked: true,
                                        display: true,
                                        gridLines: {
                                            display: false,
                                            offsetGridLines: true
                                        }
                                    }
                                ],
                                xAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                            callback(value, index, values) {
                                                return value + ' €';
                                            }
                                        }
                                    }
                                ]
                            }
                        };
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {}

    ngOnChanges() {
        this.loadAll();
    }

    ngOnDestroy() {}

    isGreen(): boolean {
        if (!this.operationADate || !this.budgetADate) {
            return true;
        }
        return this.getDeltaRatio() >= 0;
    }

    isOrange(): boolean {
        return !this.isGreen() && this.getDeltaRatio() >= -0.2;
    }

    isRed(): boolean {
        return !this.isGreen() && !this.isOrange();
    }

    getDeltaRatio() {
        return (this.operationADate - this.budgetADate) / Math.abs(this.budgetADate);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
