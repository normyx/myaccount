import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { MyaDashboardUIComponentsService } from './mya-dashboard-ui-components.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as moment from 'moment';

@Component({
    selector: 'jhi-mya-amount-category-split',
    templateUrl: './mya-amount-category-split.component.html'
})
export class MyaAmountCategorySplitComponent implements OnInit, OnChanges, OnDestroy {
    @Input() categoryId: number;
    @Input() month: Date;
    @Input() numberOfMonth: number;
    @Input() height: string;
    data: any;
    options: any;
    eventSubscriber: Subscription;

    constructor(
        private dashboardUIComponentsService: MyaDashboardUIComponentsService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService
    ) {}

    private getCategoryColors(n: number) {
        return [
            '#F44336',
            '#9C27B0',
            '#FFC107',
            '#673AB7',
            '#009688',
            '#4CAF50',
            '#607D8B',
            '#CDDC39',
            '#795548',
            '#9E9E9E',
            '#E91E63',
            '#FF9800'
        ].slice(0, n);
    }

    loadAll() {
        if (this.categoryId && this.month && this.numberOfMonth) {
            this.dashboardUIComponentsService.getSubCategorySplit(this.categoryId, moment(this.month), this.numberOfMonth).subscribe(
                (res: HttpResponse<any>) => {
                    this.data = {
                        labels: res.body.categoryNames,
                        datasets: [
                            {
                                data: res.body.amounts,
                                backgroundColor: this.getCategoryColors(res.body.amounts.length)
                            }
                        ]
                    };
                    this.options = {
                        responsive: true,
                        /* title: {
                            display: true,
                            text: 'Moyenne sur ' + this.numberOfMonth + ' dernier(s) mois',
                            fontSize: 12
                        }, */
                        animation: {
                            animateScale: true
                        },
                        legend: {
                            display: false,
                            position: 'bottom'
                        },
                        tooltips: {
                            callbacks: {
                                label: (tooltipItem, data) => {
                                    const dataset = data.datasets[tooltipItem.datasetIndex];
                                    let label = data.labels[tooltipItem.index] || '';
                                    if (label) {
                                        label += ' : ';
                                    }
                                    label += Math.round(dataset.data[tooltipItem.index] * 100) / 100;
                                    label += ' €';
                                    return label;
                                }
                            }
                        }
                    };
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        }
    }

    ngOnInit() {
        this.registerChangeInBudgetItems();
        this.loadAll();
    }

    ngOnChanges() {
        this.loadAll();
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }
    registerChangeInBudgetItems() {
        this.eventSubscriber = this.eventManager.subscribe('myaAmountCategorySplit' + this.categoryId ? this.categoryId : '', response =>
            this.loadAll()
        );
    }
    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
