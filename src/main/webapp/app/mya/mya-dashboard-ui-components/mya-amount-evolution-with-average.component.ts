import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { MyaDashboardUIComponentsService } from './mya-dashboard-ui-components.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as moment from 'moment';
import { ICategory } from 'app/shared/model/category.model';

@Component({
    selector: 'jhi-mya-amount-evolution-with-average',
    templateUrl: './mya-amount-evolution-with-average.component.html'
})
export class MyaAmountEvolutionWithAverageComponent implements OnInit, OnChanges {
    @Input() category: ICategory;
    @Input() monthTo: Date;
    @Input() monthFrom: Date;
    @Input() height: string;
    data: any;
    options: any;
    eventSubscriber: Subscription;

    constructor(
        private dashboardUIComponentsService: MyaDashboardUIComponentsService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService
    ) {}

    loadAll() {
        if (this.monthTo && this.monthFrom) {
            const categoryId = this.category ? this.category.id : null;
            this.dashboardUIComponentsService.getAmountCategoryPerMonth(categoryId, moment(this.monthFrom), moment(this.monthTo)).subscribe(
                (res: HttpResponse<any>) => {
                    this.data = {
                        labels: res.body.months,
                        datasets: [
                            {
                                label: 'Montant',
                                data: res.body.amounts,
                                borderColor: '#49ab81',
                                backgroundColor: '#49ab81',
                                fill: false,
                                pointRadius: 0
                            },
                            {
                                label: 'Budget',
                                data: res.body.budgetAmounts,
                                borderColor: '#3b5998',
                                backgroundColor: '#3b5998',
                                fill: false,
                                pointRadius: 0
                            },
                            {
                                label: 'Montant Moy. 3 mois',
                                data: res.body.amountsAvg3,
                                borderColor: '#8b9dc3',
                                backgroundColor: '#8b9dc3',
                                // borderColor: '##35bf4d',
                                fill: false,
                                // borderDash: [5, 5],
                                borderWidth: 2,
                                pointRadius: 0
                            },
                            {
                                label: 'Montant Moy. 12 mois',
                                data: res.body.amountsAvg12,
                                borderColor: '#dfe3ee',
                                backgroundColor: '#dfe3ee',
                                // borderColor: '##35bf4d',
                                fill: false,
                                // borderDash: [10, 10],
                                borderWidth: 2,
                                pointRadius: 0
                            }
                        ]
                    };
                    this.options = {
                        responsive: true,
                        /* title: {
                                display: true,
                                text: 'Evolutions',
                                fontSize: 12
                            }, */
                        legend: {
                            display: false,
                            position: 'bottom'
                        },
                        scales: {
                            yAxes: [
                                {
                                    scaleLabel: {
                                        display: false,
                                        labelString: 'Montants'
                                    },
                                    ticks: {
                                        suggestedMax: 0,
                                        callback(value, index, values) {
                                            return value + ' €';
                                        }
                                    }
                                }
                            ],
                            xAxes: [
                                {
                                    display: false
                                }
                            ]
                        },
                        tooltips: {
                            position: 'average',
                            mode: 'index',
                            intersect: false,
                            callbacks: {
                                label: (tooltipItem, data) => {
                                    let label = data.datasets[tooltipItem.datasetIndex].label || '';
                                    if (label) {
                                        label += ' : ';
                                    }
                                    label += Math.round(tooltipItem.yLabel * 100) / 100;
                                    label += ' €';
                                    return label;
                                }
                            }
                        }
                    };
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        }
    }

    ngOnInit() {}

    ngOnChanges() {
        this.loadAll();
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
