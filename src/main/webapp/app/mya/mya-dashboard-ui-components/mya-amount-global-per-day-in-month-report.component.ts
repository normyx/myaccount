import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { MyaDashboardUIComponentsService } from './mya-dashboard-ui-components.service';
// import { IAccountCategoryMonthReport } from './account-category-month-report.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ChartModule } from 'primeng/chart';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as moment from 'moment';
import { ICategory } from 'app/shared/model/category.model';

@Component({
    selector: 'jhi-mya-amount-global-per-day-in-month-report',
    templateUrl: './mya-amount-global-per-day-in-month-report.component.html'
})
export class MyaAmountGlobalPerDayInMonthReportComponent implements OnInit, OnChanges {
    @Input() month: Date;
    @Input() categoryId: number;
    @Input() height = '30vh';
    // accountCategoryMonthReport: IAccountCategoryMonthReport;
    data: any;
    options: any;

    constructor(private dashboardUIComponentsService: MyaDashboardUIComponentsService, private jhiAlertService: JhiAlertService) {}

    private feedReportData(res: HttpResponse<any>) {
        this.data = {
            labels: res.body.dates,
            datasets: [
                {
                    label: 'Operation',
                    data: res.body.operationAmounts,
                    borderColor: '#0099ff',
                    backgroundColor: '#0099ff',
                    fill: false,
                    pointRadius: 0
                },
                {
                    label: 'Budget',
                    data: res.body.budgetAmounts,
                    borderColor: '#565656',
                    backgroundColor: '#565656',
                    borderWidth: 1,
                    fill: false,
                    pointRadius: 0
                },
                {
                    label: 'Evolution prévue',
                    data: res.body.predictiveBudgetAmounts,
                    borderColor: '#ff0000',
                    backgroundColor: '#ff0000',
                    fill: false,
                    pointRadius: 0
                }
            ]
        };
        this.options = {
            /* title: {
                display: true,
                text: res.body.month,
                fontSize: 12
            }, */
            legend: {
                display: false,
                position: 'bottom'
            },
            scales: {
                yAxes: [
                    {
                        scaleLabel: {
                            display: false,
                            labelString: 'Montants'
                        },
                        ticks: {
                            suggestedMax: 0,
                            callback(value, index, values) {
                                return value + ' €';
                            }
                        }
                    }
                ],
                xAxes: [
                    {
                        display: false
                    }
                ]
            },
            tooltips: {
                position: 'average',
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: (tooltipItem, data) => {
                        let label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if (label) {
                            label += ' : ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        label += ' €';
                        return label;
                    }
                }
            }
        };
    }

    loadAll() {
        if (this.month) {
            this.dashboardUIComponentsService
                .getAmountGlobalPerDayInMonth(moment(this.month), this.categoryId)
                .subscribe((res: HttpResponse<any>) => this.feedReportData(res), (res: HttpErrorResponse) => this.onError(res.message));
        }
    }

    ngOnInit() {}

    ngOnChanges() {
        this.loadAll();
    }
    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
