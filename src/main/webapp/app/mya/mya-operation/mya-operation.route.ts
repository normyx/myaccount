import { Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { MyaOperationComponent } from './mya-operation.component';

import { IOperation } from 'app/shared/model/operation.model';

export const myaOperationRoute: Routes = [
    {
        path: 'mya-operation',
        component: MyaOperationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Suivi des Operations'
        },
        canActivate: [UserRouteAccessService]
    }
];
