import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOperation } from 'app/shared/model/operation.model';

type EntityResponseType = HttpResponse<IOperation>;
type EntityArrayResponseType = HttpResponse<IOperation[]>;

@Injectable({ providedIn: 'root' })
export class MyaOperationService {
    private resourceUrl = SERVER_API_URL + 'api/operations';
    private resourceCloseToBudgetItemPeriodUrl = SERVER_API_URL + 'api/operations-close-to-budget';
    private resourceCloseToBudgetItemPeriodWithoutAlreadyAssignedUrl =
        SERVER_API_URL + 'api/operations-close-to-budget-without-already-assigned';

    constructor(private http: HttpClient) {}

    queryWithUser(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IOperation[]>(SERVER_API_URL + 'api/operations-with-user', { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }
    findCloseToBudgetItemPeriod(budgetItemPeriodId: number): Observable<EntityArrayResponseType> {
        return this.http
            .get<IOperation[]>(`${this.resourceCloseToBudgetItemPeriodUrl}/${budgetItemPeriodId}`, { observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    findCloseToBudgetItemPeriodWithoutAlreadyAssigned(budgetItemPeriodId: number): Observable<EntityArrayResponseType> {
        return this.http
            .get<IOperation[]>(`${this.resourceCloseToBudgetItemPeriodWithoutAlreadyAssignedUrl}/${budgetItemPeriodId}`, {
                observe: 'response'
            })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    countCloseToBudgetItemPeriod(budgetItemPeriodId: number): Observable<HttpResponse<number>> {
        return this.http.get<number>(`api/count-operations-close-to-budget/${budgetItemPeriodId}`, { observe: 'response' });
    }

    importOperationsFiles(): Observable<void> {
        return this.http.put<void>(`api/import-operations-file`, { observe: 'response' });
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((operation: IOperation) => {
                operation.date = operation.date != null ? moment(operation.date) : null;
            });
        }
        return res;
    }
}
