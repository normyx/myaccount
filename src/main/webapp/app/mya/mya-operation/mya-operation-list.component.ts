import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges, SimpleChange, Input } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription, Subject } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IOperation } from 'app/shared/model/operation.model';
import { ICategory } from 'app/shared/model/category.model';

import { ITEMS_PER_PAGE } from 'app/shared';
import { MyaOperationService } from './mya-operation.service';
import { SubCategoryService } from 'app/entities/sub-category/sub-category.service';
import * as Moment from 'moment';
import { ISubCategory } from 'app/shared/model/sub-category.model';

@Component({
    selector: 'jhi-mya-operation-list',
    templateUrl: './mya-operation-list.component.html'
})
export class MyaOperationListComponent implements OnInit, OnDestroy, OnChanges {
    operations: IOperation[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    subCategoriesIdMap: Map<string, number[]>;
    @Input()
    dateFrom: Date;
    @Input()
    dateTo: Date;
    @Input()
    filterSelectedCategory: ICategory;
    @Input()
    amountFrom: number;
    @Input()
    amountTo: number;
    @Input()
    monthOnly = false;
    subCategoriesSubject = new Subject<Map<string, number[]>>();

    constructor(
        private operationService: MyaOperationService,
        private subCategoryService: SubCategoryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks
    ) {
        this.operations = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'date';
        this.reverse = false;
    }

    loadSubCategories() {
        this.subCategoryService
            .query()
            .toPromise()
            .then(
                (res: HttpResponse<ISubCategory[]>) => {
                    this.subCategoriesIdMap = new Map();
                    const allSubCat: number[] = [];
                    this.subCategoriesIdMap.set('all', allSubCat);
                    const subCategories: ISubCategory[] = res.body;
                    for (const subCat of subCategories) {
                        const subCatId = subCat.id;
                        const catId = subCat.categoryId;
                        let subCatIds = this.subCategoriesIdMap.get('' + catId);
                        if (!subCatIds) {
                            subCatIds = [];
                            this.subCategoriesIdMap.set('' + catId, subCatIds);
                        }
                        subCatIds.push(subCatId);
                        allSubCat.push(subCatId);
                    }
                    this.subCategoriesSubject.next(this.subCategoriesIdMap);
                    this.subCategoriesSubject.complete();
                    this.loadAll();
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadAll() {
        if (this.subCategoriesIdMap) {
            const query = {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            };
            if (this.dateFrom) {
                query['date.greaterOrEqualThan'] = Moment(this.dateFrom).format('YYYY-MM-DD');
                if (this.monthOnly) {
                    query['date.lessOrEqualThan'] = Moment(new Date(this.dateFrom.getFullYear(), this.dateFrom.getMonth() + 1, 0)).format(
                        'YYYY-MM-DD'
                    );
                } else {
                    if (this.dateTo) {
                        query['date.lessOrEqualThan'] = Moment(this.dateTo).format('YYYY-MM-DD');
                    }
                }
            }
            if (this.filterSelectedCategory) {
                query['subCategoryId.in'] = this.subCategoriesIdMap.get('' + this.filterSelectedCategory.id);
            }
            if (this.amountFrom) {
                query['amount.greaterOrEqualThan'] = this.amountFrom;
            }
            if (this.amountTo) {
                query['amount.lessOrEqualThan'] = this.amountTo;
            }
            this.operationService
                .queryWithUser(query)
                .subscribe(
                    (res: HttpResponse<IOperation[]>) => this.paginateOperations(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
        }
    }

    reset() {
        this.page = 0;
        this.operations = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.reset();
    }

    ngOnInit() {
        this.loadSubCategories();
        // this.loadAll();
        this.registerChangeInOperations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IOperation) {
        return item.id;
    }

    registerChangeInOperations() {
        this.eventSubscriber = this.eventManager.subscribe('myaOperationListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateOperations(data: IOperation[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.operations.push(data[i]);
        }
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
