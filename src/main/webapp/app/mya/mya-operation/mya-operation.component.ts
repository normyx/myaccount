import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { MyaOperationService } from './mya-operation.service';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

@Component({
    selector: 'jhi-mya-operation',
    templateUrl: './mya-operation.component.html'
})
export class MyaOperationComponent implements OnInit, OnDestroy {
    dateFrom = new FormControl();
    dateTo = new FormControl();
    filterCategories: ICategory[];
    categoryId: number;
    filterSelectedCategory: ICategory;
    amount: number;
    amountInterval = 0;
    refreshOn = false;

    constructor(
        private categoryService: CategoryService,
        private myaOperationService: MyaOperationService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager
    ) {}

    loadAll() {}

    ngOnInit() {
        this.categoryService.query().subscribe(
            (res: HttpResponse<ICategory[]>) => {
                this.filterCategories = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnDestroy() {}

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    getLeftAmountInterval() {
        if (this.amount) {
            return this.amount - this.amountInterval;
        }
        return null;
    }

    getRightAmountInterval() {
        if (this.amount) {
            return this.amount + this.amountInterval;
        }
        return null;
    }

    importOperationsFiles() {
        this.refreshOn = true;
        this.myaOperationService.importOperationsFiles().subscribe((res: void) => {
            this.refreshOn = false;
            this.eventManager.broadcast({ name: 'myaOperationListModification', content: 'OK' });
        });
    }

    onFileComplete(data: any) {
        console.warn(data); // We just print out data bubbled up from event emitter.
    }
}
