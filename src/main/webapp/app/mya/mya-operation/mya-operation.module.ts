import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyaccountSharedModule } from 'app/shared';
import { MyaccountAdminModule } from 'app/admin/admin.module';
import { MyaCategoryIconModule } from 'app/mya/mya-category-icon/mya-category-icon.module';

import { MyaOperationComponent, myaOperationRoute } from './';
import { MyaOperationListComponent } from './mya-operation-list.component';
import { MyaMaterialFileUploadModule } from '../mya-material-file-upload/mya-material-file-upload.module';

const ENTITY_STATES = [...myaOperationRoute];

@NgModule({
    imports: [
        MyaccountSharedModule,
        MyaccountAdminModule,
        MyaCategoryIconModule,
        MyaMaterialFileUploadModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [MyaOperationComponent, MyaOperationListComponent],
    entryComponents: [MyaOperationComponent, MyaOperationListComponent],
    exports: [MyaOperationListComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaccountMyaOperationModule {}
