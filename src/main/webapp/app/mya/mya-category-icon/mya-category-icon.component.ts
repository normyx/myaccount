import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'jhi-mya-category-icon',
    templateUrl: './mya-category-icon.component.html',
    styleUrls: ['mya-category-icon.scss']
})
export class MyaCategoryIconComponent implements OnInit {
    @Input()
    categoryId: string;
    @Input()
    iconSize: string;

    constructor() {}

    ngOnInit() {}

    getCategoryIcon(): string {
        let iconString: string;
        switch (this.categoryId) {
            case '1': {
                iconString = 'home';
                break;
            }
            case '2': {
                iconString = 'shopping_cart';
                break;
            }
            case '3': {
                iconString = 'account_balance';
                break;
            }
            case '4': {
                iconString = 'child_care';
                break;
            }
            case '5': {
                iconString = 'gamepad';
                break;
            }
            case '6': {
                iconString = 'school';
                break;
            }
            case '7': {
                iconString = 'restaurant';
                break;
            }
            case '8': {
                iconString = 'directions_car';
                break;
            }
            case '9': {
                iconString = 'room_service';
                break;
            }
            case '10': {
                iconString = 'local_hospital';
                break;
            }
            case '11': {
                iconString = 'euro_symbol';
                break;
            }
            case '12': {
                iconString = 'card_travel';
                break;
            }
            case '13': {
                iconString = 'category';
                break;
            }
            default: {
                iconString = 'close';
                break;
            }
        }
        return iconString;
    }
}
