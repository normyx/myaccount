import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyaccountSharedModule } from 'app/shared';
import { MyaccountAdminModule } from 'app/admin/admin.module';
import { MyaMaterialFileUploadComponent } from './mya-material-file-upload.component';

@NgModule({
    imports: [MyaccountSharedModule, MyaccountAdminModule, CommonModule],
    declarations: [MyaMaterialFileUploadComponent],
    exports: [MyaMaterialFileUploadComponent],
    entryComponents: [MyaMaterialFileUploadComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyaMaterialFileUploadModule {}
