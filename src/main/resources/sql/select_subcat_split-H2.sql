SELECT 
    cat.category_name, subcat.sub_category_name, SUM(op.amount) / :numberOfMonths
FROM
    operation op
        JOIN
    sub_category AS subcat ON subcat.id = op.sub_category_id
        JOIN
    category AS cat ON cat.id = subcat.category_id
WHERE
    op.account_id = :accountId AND cat.id = :categoryId
        AND op.jhi_date >= DATEADD('MONTH',0-(:numberOfMonths+1), DATEADD('DAY', 1, DATEADD(dd, -DAY(DATEADD(m,1,:month)), DATEADD(m,1,:month))))
        AND op.jhi_date <= DATEADD(dd, -DAY(DATEADD(m,1,:month)), :month)
GROUP BY op.sub_category_id
ORDER BY cat.id , subcat.id