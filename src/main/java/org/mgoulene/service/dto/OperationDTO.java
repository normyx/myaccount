package org.mgoulene.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Operation entity.
 */
public class OperationDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 400)
    private String label;

    @NotNull
    private LocalDate date;

    @NotNull
    private Float amount;

    @Size(max = 400)
    private String note;

    @Size(max = 20)
    private String checkNumber;

    @NotNull
    private Boolean isUpToDate;

    private Boolean deletingHardLock;


    private Long subCategoryId;

    private String subCategorySubCategoryName;

    private Long accountId;

    private String accountLogin;

    private Long bankAccountId;

    private String bankAccountName;

    private String bankAccountShortName;

    private Long categoryId;

    private Long budgetItemPeriodId;

    /**
     * @return the budgetItemPeriodId
     */
    public Long getBudgetItemPeriodId() {
        return budgetItemPeriodId;
    }


    /**
     * @param budgetItemPeriodId the budgetItemPeriodId to set
     */
    public void setBudgetItemPeriodId(Long budgetItemPeriodId) {
        this.budgetItemPeriodId = budgetItemPeriodId;
    }

    /**
     * @return the categoryId
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Boolean isIsUpToDate() {
        return isUpToDate;
    }

    public void setIsUpToDate(Boolean isUpToDate) {
        this.isUpToDate = isUpToDate;
    }

    public Boolean isDeletingHardLock() {
        return deletingHardLock;
    }

    public void setDeletingHardLock(Boolean deletingHardLock) {
        this.deletingHardLock = deletingHardLock;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategorySubCategoryName() {
        return subCategorySubCategoryName;
    }

    public void setSubCategorySubCategoryName(String subCategorySubCategoryName) {
        this.subCategorySubCategoryName = subCategorySubCategoryName;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long userId) {
        this.accountId = userId;
    }

    public String getAccountLogin() {
        return accountLogin;
    }

    public void setAccountLogin(String userLogin) {
        this.accountLogin = userLogin;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

        /**
     * @return the bankAccountShortName
     */
    public String getBankAccountShortName() {
        return bankAccountShortName;
    }

    /**
     * @param bankAccountShortName the bankAccountShortName to set
     */
    public void setBankAccountShortName(String bankAccountShortName) {
        this.bankAccountShortName = bankAccountShortName;
    }

    /**
     * @return the bankAccountName
     */
    public String getBankAccountName() {
        return bankAccountName;
    }

    /**
     * @param bankAccountName the bankAccountName to set
     */
    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

     @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OperationDTO operationDTO = (OperationDTO) o;
        if (operationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), operationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OperationDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", date='" + getDate() + "'" +
            ", amount=" + getAmount() +
            ", note='" + getNote() + "'" +
            ", checkNumber='" + getCheckNumber() + "'" +
            ", isUpToDate='" + isIsUpToDate() + "'" +
            ", deletingHardLock='" + isDeletingHardLock() + "'" +
            ", subCategory=" + getSubCategoryId() +
            ", category=" + getCategoryId() +
            ", subCategory='" + getSubCategorySubCategoryName() + "'" +
            ", account=" + getAccountId() +
            ", account='" + getAccountLogin() + "'" +
            ", bankAccount=" + getBankAccountId() +
            "}";
    }

    

    
    public boolean sameOperation(OperationDTO obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        
        OperationDTO other = (OperationDTO) obj;
        if (accountId == null) {
            if (other.accountId != null)
                return false;
        } else if (!accountId.equals(other.accountId))
            return false;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (amount.intValue() != other.amount.intValue())
            return false;
        if (bankAccountId == null) {
            if (other.bankAccountId != null)
                return false;
        } else if (!bankAccountId.equals(other.bankAccountId))
            return false;
        if (checkNumber == null) {
            if (other.checkNumber != null)
                return false;
        } else if (!checkNumber.equals(other.checkNumber))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (note == null) {
            if (other.note != null)
                return false;
        } else if (!note.equals(other.note))
            return false;
        if (subCategoryId == null) {
            if (other.subCategoryId != null)
                return false;
        } else if (!subCategoryId.equals(other.subCategoryId))
            return false;
        return true;
    }
}
