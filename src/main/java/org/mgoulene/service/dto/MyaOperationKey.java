package org.mgoulene.service.dto;

import java.time.LocalDate;

import org.simmetrics.StringMetric;
import org.simmetrics.metrics.StringMetrics;

public class MyaOperationKey {
    private LocalDate date;
    private Float amount;
    private String label;
    private Long accountId;
    private int hash = -1;

    private static StringMetric metric = StringMetrics.cosineSimilarity();

    public MyaOperationKey(LocalDate date, Float amount, String label, Long accountId) {
        this.date = date;
        this.amount = new Float(amount.intValue());
        this.label = label;
        this.accountId = accountId;
    }

    public MyaOperationKey(OperationDTO operationDTO) {
        this(operationDTO.getDate(),operationDTO.getAmount(),operationDTO.getLabel(),operationDTO.getAccountId());
    }

    @Override
    public int hashCode() {
        if (hash == -1) {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
            result = prime * result + ((amount == null) ? 0 : amount.hashCode());
            result = prime * result + ((date == null) ? 0 : date.hashCode());
            this.hash = result;
        }
        // result = prime * result + ((label == null) ? 0 : label.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MyaOperationKey other = (MyaOperationKey) obj;
        if (accountId == null) {
            if (other.accountId != null)
                return false;
        } else if (!accountId.equals(other.accountId))
            return false;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (!amount.equals(other.amount))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (metric.compare(label, other.label) < 0.7)
            return false;
        return true;
    }
}