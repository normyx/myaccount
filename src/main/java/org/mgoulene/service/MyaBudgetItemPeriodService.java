package org.mgoulene.service;

import java.time.LocalDate;
import java.util.List;

import org.mgoulene.domain.BudgetItem;
import org.mgoulene.domain.BudgetItemPeriod;
import org.mgoulene.repository.BudgetItemPeriodRepository;
import org.mgoulene.repository.MyaAvailableDateRepository;
import org.mgoulene.service.dto.BudgetItemDTO;
import org.mgoulene.service.dto.BudgetItemPeriodDTO;
import org.mgoulene.service.mapper.BudgetItemMapper;
import org.mgoulene.service.mapper.BudgetItemPeriodMapper;
import org.mgoulene.web.rest.util.LocalDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing BudgetItemPeriod.
 */
@Service
@Transactional
public class MyaBudgetItemPeriodService extends BudgetItemPeriodService {

    private final Logger log = LoggerFactory.getLogger(MyaBudgetItemPeriodService.class);

    private final BudgetItemMapper budgetItemMapper;

    private final MyaAvailableDateRepository availableDateRepository;

    public MyaBudgetItemPeriodService(BudgetItemPeriodRepository budgetItemPeriodRepository,
            BudgetItemPeriodMapper budgetItemPeriodMapper, BudgetItemMapper budgetItemMapper,
            MyaAvailableDateRepository availableDateRepository) {
        super(budgetItemPeriodRepository, budgetItemPeriodMapper);
        this.budgetItemMapper = budgetItemMapper;
        this.availableDateRepository = availableDateRepository;

    }

    public void createWithNext(BudgetItemDTO budgetItemDTO, BudgetItemPeriodDTO budgetItemPeriodDTO, LocalDate from,
            int dayOfMonth) {
        budgetItemPeriodDTO.setId(0L);
        BudgetItem budgetItem = budgetItemMapper.toEntity(budgetItemDTO);
        List<LocalDate> months = availableDateRepository.findAllMonthFrom(from);
        // Create all the BudgetItemPeriod from the parameter start month
        for (LocalDate month : months) {
            BudgetItemPeriod bip = budgetItemPeriodMapper.toEntity(budgetItemPeriodDTO);
            bip.setMonth(month);
            bip.setBudgetItem(budgetItem);
            if (!budgetItemPeriodDTO.isIsSmoothed()) {
                bip.setDate(LocalDateUtil.getLocalDate(month, dayOfMonth));
            }
            budgetItemPeriodRepository.save(bip);
        }

    }

    public BudgetItemPeriodDTO findLastBudgetItemPeriod(Long budgetItemId) {
        log.debug("Request to findLastBudgetItemPeriod BudgetItemId : {}", budgetItemId);
        BudgetItemPeriod bip = budgetItemPeriodRepository.findLastBudgetItemPeriod(budgetItemId);

        return budgetItemPeriodMapper.toDto(bip);
    }

    public void extendWithNext(BudgetItemDTO budgetItemDTO) {
        log.debug("Request to extend BudgetItem : {}", budgetItemDTO);
        BudgetItemPeriodDTO budgetItemPeriodDTO = findLastBudgetItemPeriod(budgetItemDTO.getId());
        createWithNext(budgetItemDTO, budgetItemPeriodDTO, budgetItemPeriodDTO.getMonth().plusMonths(1),
                budgetItemPeriodDTO.isIsSmoothed() ? 0 : budgetItemPeriodDTO.getDate().getDayOfMonth());
    }

    /**
     * Delete BudgetItemPeriod with the next.
     *
     * @param budgetItemPeriodDTO the entity to save
     * @return the persisted entity
     */
    public void deleteWithNext(BudgetItemPeriodDTO budgetItemPeriodDTO) {
        log.debug("Request to deleteWithNext BudgetItemPeriod : {}", budgetItemPeriodDTO);

        budgetItemPeriodRepository.deleteWithNext(budgetItemPeriodDTO.getBudgetItemId(),
                budgetItemPeriodDTO.getMonth());
    }

    /**
     * Delete the budgetItemPeriod from budgetItemId.
     *
     * @param id the id of the entity
     */
    public void deleteFromBudgetItem(Long budgetItemId) {
        log.debug("Request to delete BudgetItemPeriod from BudgetItemId : {}", budgetItemId);
        budgetItemPeriodRepository.deleteFromBudgetItem(budgetItemId);
    }

}
