package org.mgoulene.repository;

import java.time.LocalDate;
import java.util.List;

import org.mgoulene.domain.MyaCategorySplit;
import org.mgoulene.domain.MyaReportAmountsByDates;
import org.mgoulene.domain.MyaReportDateEvolutionData;
import org.mgoulene.domain.MyaReportMonthlyData;

public interface MyaReportDataRepository {

        public List<MyaReportDateEvolutionData> findReportDataWhereMonth(Long accountId, LocalDate month);

        public List<MyaReportDateEvolutionData> findReportDataWhereMonthAndCategory(Long accountId, LocalDate month,
                        Long categoryId);

        public List<MyaReportMonthlyData> findMonthlyReportDataWhereCategoryBetweenMonth(Long accountId,
                        Long categoryId, LocalDate fromDate, LocalDate toDate);

        public List<MyaReportMonthlyData> findMonthlyReportDataBetweenMonth(Long accountId, LocalDate fromDate,
                        LocalDate toDate);

        public List<MyaReportDateEvolutionData> findMonthlyReportDataWhereCategoryBetweenMonthWithUnmarked(
                        Long accountId, Long categoryId, LocalDate fromDate, LocalDate toDate);

        public List<MyaCategorySplit> findSubCategorySplit(Long accountId, Long categoryId, LocalDate month,
                        int numberOfMonths);

        public List<MyaReportAmountsByDates> findAmountsBetweenDates(Long accountId, LocalDate dateFrom,
                        LocalDate dateTo);
}
