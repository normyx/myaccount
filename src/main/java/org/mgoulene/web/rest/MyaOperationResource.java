package org.mgoulene.web.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.mgoulene.domain.User;
import org.mgoulene.service.BudgetItemPeriodService;
import org.mgoulene.service.BudgetItemService;
import org.mgoulene.service.MyaOperationCSVImporterService;
import org.mgoulene.service.MyaOperationService;
import org.mgoulene.service.OperationQueryService;
import org.mgoulene.service.UserService;
import org.mgoulene.service.dto.BudgetItemDTO;
import org.mgoulene.service.dto.BudgetItemPeriodDTO;
import org.mgoulene.service.dto.OperationCriteria;
import org.mgoulene.service.dto.OperationDTO;
import org.mgoulene.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.github.jhipster.service.filter.LongFilter;

/**
 * REST controller for managing Operation.
 */
@RestController
@RequestMapping("/api")
public class MyaOperationResource {

    private final Logger log = LoggerFactory.getLogger(MyaOperationResource.class);

    private final MyaOperationService operationService;

    private final OperationQueryService operationQueryService;

    private final BudgetItemPeriodService budgetItemPeriodService;

    private final BudgetItemService budgetItemService;

    private final MyaOperationCSVImporterService operationCSVImporterService;

    private final UserService userService;

    public MyaOperationResource(MyaOperationService operationService, OperationQueryService operationQueryService,
            BudgetItemPeriodService budgetItemPeriodService, BudgetItemService budgetItemService,
            MyaOperationCSVImporterService operationCSVImporterService, UserService userService) {
        this.operationService = operationService;
        this.operationQueryService = operationQueryService;
        this.operationCSVImporterService = operationCSVImporterService;
        this.budgetItemService = budgetItemService;
        this.budgetItemPeriodService = budgetItemPeriodService;
        this.userService = userService;
    }

    private ResponseEntity<List<OperationDTO>> getOperationCloseToBudgetItemPeriod(
            @PathVariable Long budgetItemPeriodId, boolean withAlreadyAssigned) {
        log.debug("REST request to get Operation close to budgetPeriodId : {}, withAlreadyAssigned : {}",
                budgetItemPeriodId, withAlreadyAssigned);
        Optional<BudgetItemPeriodDTO> budgetItemPeriodDTOOptional = budgetItemPeriodService.findOne(budgetItemPeriodId);
        if (budgetItemPeriodDTOOptional.isPresent()) {
            BudgetItemPeriodDTO budgetItemPeriodDTO = budgetItemPeriodDTOOptional.get();
            Optional<BudgetItemDTO> budgetItemDTOOptional = budgetItemService
                    .findOne(budgetItemPeriodDTO.getBudgetItemId());
            if (budgetItemDTOOptional.isPresent()) {
                BudgetItemDTO budgetItemDTO = budgetItemDTOOptional.get();
                Long accoundId = budgetItemDTO.getAccountId();
                Long categoryId = budgetItemDTO.getCategoryId();
                Float amount = budgetItemPeriodDTO.getAmount().floatValue();
                LocalDate fromDate = budgetItemPeriodDTO.getDate().minusDays(20);
                LocalDate toDate = budgetItemPeriodDTO.getDate().plusDays(20);
                List<OperationDTO> operations;
                if (withAlreadyAssigned) {
                    operations = operationService.findAllCloseToBudgetItemPeriod(accoundId, categoryId, amount,
                            fromDate, toDate);
                } else {
                    operations = operationService.findAllCloseToBudgetItemPeriodWithoutAlreadyAssigned(accoundId,
                            categoryId, amount, fromDate, toDate);
                }
                return new ResponseEntity<>(operations, HttpStatus.OK);
            } else {
                log.error("REST request cannot find BudgetItem : {}", budgetItemPeriodDTO.getBudgetItemId());
            }
        } else {
            log.error("REST request cannot find BudgetItemPeriod : {}", budgetItemPeriodId);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET /operations-close-to-budget/:budget-item-period-id : get the "id"
     * operation.
     *
     * @param id the id of the budgetItemPeriodId to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         operationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/operations-close-to-budget/{budgetItemPeriodId}")
    public ResponseEntity<List<OperationDTO>> getOperationCloseToBudgetItemPeriod(
            @PathVariable Long budgetItemPeriodId) {
        return getOperationCloseToBudgetItemPeriod(budgetItemPeriodId, true);
    }

    /**
     * GET
     * /operations-close-to-budget-without-already-assigned/:budget-item-period-id :
     * get the "id" operation.
     *
     * @param id the id of the budgetItemPeriodId to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         operationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/operations-close-to-budget-without-already-assigned/{budgetItemPeriodId}")
    public ResponseEntity<List<OperationDTO>> getOperationCloseToBudgetItemPeriodWithoutAlreadyAssigned(
            @PathVariable Long budgetItemPeriodId) {
        return getOperationCloseToBudgetItemPeriod(budgetItemPeriodId, false);
    }

    /**
     * GET /operations : get all the operations.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of operations in
     *         body
     */
    @GetMapping("/operations-with-user")
    public ResponseEntity<List<OperationDTO>> getAllOperationsWithUser(OperationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Operations by criteria: {}", criteria);
        Optional<User> userOptional = userService.getUserWithAuthorities();
        if (userOptional.isPresent()) {
            LongFilter userFilter = new LongFilter();
            userFilter.setEquals(userOptional.get().getId());
            criteria.setAccountId(userFilter);
            Page<OperationDTO> page = operationQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/operations");
            return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
        return null;
    }

    /**
     * GET /operations-close-to-budget/:budget-item-period-id : get the "id"
     * operation.
     *
     * @param id the id of the budgetItemPeriodId to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         operationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/count-operations-close-to-budget/{budgetItemPeriodId}")
    public ResponseEntity<Integer> countOperationCloseToBudgetItemPeriod(@PathVariable Long budgetItemPeriodId) {
        log.debug("REST request to count Operation clode to budgetPeriodId : {}", budgetItemPeriodId);
        Optional<BudgetItemPeriodDTO> budgetItemPeriodDTOOptional = budgetItemPeriodService.findOne(budgetItemPeriodId);
        if (budgetItemPeriodDTOOptional.isPresent()) {
            BudgetItemPeriodDTO budgetItemPeriodDTO = budgetItemPeriodDTOOptional.get();
            Optional<BudgetItemDTO> budgetItemDTOOptional = budgetItemService
                    .findOne(budgetItemPeriodDTO.getBudgetItemId());
            if (budgetItemDTOOptional.isPresent()) {
                if (!budgetItemPeriodDTO.isIsSmoothed() && budgetItemPeriodDTO.getOperationId() == null) {
                    BudgetItemDTO budgetItemDTO = budgetItemDTOOptional.get();

                    int count = operationService.findCountAllCloseToBudgetItemPeriodWithoutAlreadyAssigned(
                            budgetItemDTO.getAccountId(), budgetItemDTO.getCategoryId(),
                            budgetItemPeriodDTO.getAmount().floatValue(), budgetItemPeriodDTO.getDate().minusDays(20),
                            budgetItemPeriodDTO.getDate().plusDays(20));
                    return new ResponseEntity<>(count, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(0, HttpStatus.OK);
                }
            } else {
                log.error("REST request cannot find BudgetItem : {}", budgetItemPeriodDTO.getBudgetItemId());
            }
        } else {
            log.error("REST request cannot find BudgetItemPeriod : {}", budgetItemPeriodId);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/import-operations-file")
    public void importOperationCSVFileFromSFTP() throws URISyntaxException {
        operationCSVImporterService.importOperationCSVFileFromSFTP();

    }

    @PostMapping(value = "/upload-operations-file", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void uploadFile(@RequestParam(name = "file") MultipartFile multipartFile) throws IOException {
        log.debug("REST request to upload file");
        if (multipartFile == null || multipartFile.isEmpty()) {
            log.error("REST request cannot upload file");
        }
        log.debug("REST request to upload file. File size : {}", multipartFile.getSize());
        Optional<User> userOptional = userService.getUserWithAuthorities();
        if (userOptional.isPresent()) {
            operationCSVImporterService.importOperationCSVFile(userOptional.get().getId(), multipartFile.getInputStream());
        }
        //return new ResponseEntity<>(new FileInformation(multipartFile.getOriginalFilename(), multipartFile.getSize()),          HttpStatus.CREATED);

    }

}
