package org.mgoulene.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.mgoulene.service.BudgetItemPeriodQueryService;
import org.mgoulene.service.MyaBudgetItemPeriodService;
import org.mgoulene.service.dto.BudgetItemPeriodCriteria;
import org.mgoulene.service.dto.BudgetItemPeriodDTO;
import org.mgoulene.web.rest.util.HeaderUtil;
import org.mgoulene.web.rest.util.LocalDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * REST controller for managing BudgetItemPeriod.
 */
@RestController
@RequestMapping("/api")
public class MyaBudgetItemPeriodResource {

    private final Logger log = LoggerFactory.getLogger(MyaBudgetItemPeriodResource.class);

    private static final String ENTITY_NAME = "budgetItemPeriod";

    private final MyaBudgetItemPeriodService budgetItemPeriodService;

    private final BudgetItemPeriodQueryService budgetItemPeriodQueryService;

    public MyaBudgetItemPeriodResource(MyaBudgetItemPeriodService budgetItemPeriodService,
            BudgetItemPeriodQueryService budgetItemPeriodQueryService) {
        this.budgetItemPeriodService = budgetItemPeriodService;
        this.budgetItemPeriodQueryService = budgetItemPeriodQueryService;
    }

    /**
     * PUT /budget-item-periods-and-next : Updates an existing budgetItemPeriod
     * whith the next with same value.
     * 
     * @param budgetItemPeriodDTO the budgetItemPeriodDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         budgetItemPeriodDTO, or with status 400 (Bad Request) if the
     *         budgetItemPeriodDTO is not valid, or with status 500 (Internal Server
     *         Error) if the budgetItemPeriodDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/budget-item-periods-and-next/{day}/{next}")
    public ResponseEntity<Void> updateBudgetItemPeriodAndNext(
            @Valid @RequestBody BudgetItemPeriodDTO budgetItemPeriodDTO, @PathVariable(name = "day") Integer day,
            @PathVariable(name = "next") boolean withNext) {
        log.debug("REST request to update BudgetItemPeriod and next : {}", budgetItemPeriodDTO);
        // Modifify the first one
        if (!budgetItemPeriodDTO.isIsSmoothed()) {
            budgetItemPeriodDTO.setDate(LocalDateUtil.getLocalDate(budgetItemPeriodDTO.getMonth(), day));
        }
        budgetItemPeriodService.save(budgetItemPeriodDTO);
        // Gets all BudgetPeriodAndNext
        if (budgetItemPeriodDTO.isIsRecurrent()) {
            BudgetItemPeriodCriteria criteria = new BudgetItemPeriodCriteria();
            // Filter on budget ID
            LongFilter biIdF = new LongFilter();
            biIdF.setEquals(budgetItemPeriodDTO.getBudgetItemId());
            criteria.setBudgetItemId(biIdF);
            // Filter for date greater that date
            LocalDateFilter biMonthF = new LocalDateFilter();
            // Find next month
            biMonthF.setGreaterOrEqualThan(budgetItemPeriodDTO.getMonth().plusMonths(1));
            if (!withNext) {
                biMonthF.setLessOrEqualThan(budgetItemPeriodDTO.getMonth());
            }
            criteria.setMonth(biMonthF);
            LongFilter opFilter = new LongFilter();
            opFilter.setSpecified(false);
            criteria.setOperationId(opFilter);
            // Filter on recurrent period only
            BooleanFilter isRecurrentF = new BooleanFilter();
            isRecurrentF.setEquals(true);
            criteria.setIsRecurrent(isRecurrentF);
            List<BudgetItemPeriodDTO> allBudgetItemPeriodsfromMonth = budgetItemPeriodQueryService
                    .findByCriteria(criteria);
            for (BudgetItemPeriodDTO bip : allBudgetItemPeriodsfromMonth) {
                bip.setAmount(budgetItemPeriodDTO.getAmount());
                bip.setIsSmoothed(budgetItemPeriodDTO.isIsSmoothed());
                if (!bip.isIsSmoothed()) {
                    bip.setDate(LocalDateUtil.getLocalDate(bip.getMonth(), day));
                }
            }
            budgetItemPeriodService.save(allBudgetItemPeriodsfromMonth);
        } 
        return ResponseEntity.ok().build();

    }

    /**
     * DELETE /budget-item-periods/:id : delete the "id" budgetItemPeriod.
     *
     * @param id the id of the budgetItemPeriodDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/budget-item-periods-and-next/{id}")
    public ResponseEntity<Void> deleteBudgetItemPeriodAndNext(@PathVariable Long id) {
        log.debug("REST request to delete BudgetItemPeriod : {}", id);
        Optional<BudgetItemPeriodDTO> budgetItemPeriodDTOOptional = budgetItemPeriodService.findOne(id);
        if (budgetItemPeriodDTOOptional.isPresent()) {
            BudgetItemPeriodDTO budgetItemPeriodDTO = budgetItemPeriodDTOOptional.get();
            if (budgetItemPeriodDTO.isIsRecurrent()) {
                budgetItemPeriodService.deleteWithNext(budgetItemPeriodDTO);
            } else {
                budgetItemPeriodService.delete(budgetItemPeriodDTO.getId());
            }
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
