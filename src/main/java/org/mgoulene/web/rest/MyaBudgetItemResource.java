package org.mgoulene.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.mgoulene.domain.User;
import org.mgoulene.service.MyaBudgetItemPeriodService;
import org.mgoulene.service.MyaBudgetItemService;
import org.mgoulene.service.UserService;
import org.mgoulene.service.dto.BudgetItemDTO;
import org.mgoulene.service.dto.BudgetItemPeriodDTO;
import org.mgoulene.web.rest.errors.BadRequestAlertException;
import org.mgoulene.web.rest.util.HeaderUtil;
import org.mgoulene.web.rest.util.LocalDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing BudgetItem.
 */
@RestController
@RequestMapping("/api")
public class MyaBudgetItemResource {

    private final Logger log = LoggerFactory.getLogger(MyaBudgetItemResource.class);

    private static final String ENTITY_NAME = "budgetItem";

    private final MyaBudgetItemService budgetItemService;

    private MyaBudgetItemPeriodService budgetItemPeriodService;

    private final UserService userService;

    public MyaBudgetItemResource(MyaBudgetItemService budgetItemService, UserService userService,
            MyaBudgetItemPeriodService budgetItemPeriodService) {
        this.budgetItemService = budgetItemService;
        this.userService = userService;
        this.budgetItemPeriodService = budgetItemPeriodService;
    }

    /**
     * GET /budget-items/:id : get the "id" budgetItem.
     * 
     * @param id the id of the budgetItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         budgetItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/budget-eligible-items/{from}/{to}")
    public ResponseEntity<List<BudgetItemDTO>> getEligibleBudgetItem(@PathVariable(name = "from") LocalDate from,
            @PathVariable(name = "to") LocalDate to, @RequestParam(name = "contains", required = false) String contains,
            @RequestParam(name = "categoryId", required = false) Long categoryId) {
        log.debug("REST request to get BudgetItem : {} {} {} {}", from, to, contains, categoryId);
        Optional<User> userOptional = userService.getUserWithAuthorities();
        Optional<List<BudgetItemDTO>> entityList;
        if (userOptional.isPresent()) {
            entityList = Optional.of(budgetItemService.findAllAvailableInPeriod(from, to, userOptional.get().getId(),
                    contains, categoryId));
        } else {
            entityList = Optional.empty();
        }
        return ResponseUtil.wrapOrNotFound(entityList);
    }

    @PostMapping("/budget-items-with-periods/{is-smoothed}/{monthFrom}/{amount}/{day-in-month}")
    public ResponseEntity<BudgetItemDTO> createBudgetItemWithPeriods(@Valid @RequestBody BudgetItemDTO budgetItemDTO,
            @PathVariable(name = "monthFrom") LocalDate monthFrom,
            @PathVariable(name = "day-in-month") Integer dayInMonth,
            @PathVariable(name = "is-smoothed") Boolean isSmoothed, @PathVariable(name = "amount") Float amount)
            throws URISyntaxException {
        log.debug("REST request to save BudgetItem with period : {} {} {} {}", budgetItemDTO, monthFrom, isSmoothed,
                amount);
        if (budgetItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new budgetItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<User> existingUser = userService.getUserWithAuthorities();
        if (existingUser.isPresent()) {
            budgetItemDTO.setAccountId(existingUser.get().getId());
        }

        BudgetItemPeriodDTO budgetItemPeriodDTO = new BudgetItemPeriodDTO();
        budgetItemPeriodDTO.setMonth(monthFrom);
        budgetItemPeriodDTO.setAmount(amount);
        budgetItemPeriodDTO.setIsSmoothed(isSmoothed);
        budgetItemPeriodDTO.setIsRecurrent(true);
        if (dayInMonth != null && !isSmoothed) {
            budgetItemPeriodDTO.setDate(LocalDateUtil.getLocalDate(monthFrom, dayInMonth));
        }
        budgetItemDTO.setOrder(budgetItemService.findNewOrder(budgetItemDTO.getAccountId()));
        BudgetItemDTO result = budgetItemService.saveWithBudgetItemPeriod(budgetItemDTO, budgetItemPeriodDTO);
        return ResponseEntity.created(new URI("/api/budget-items/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * DELETE /budget-items-with-periods/:id : delete the "id" budgetItem with the
     * budgetItemPeriod.
     *
     * @param id the id of the budgetItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/budget-items-with-periods/{id}")
    public ResponseEntity<Void> deleteBudgetItemWithPeriods(@PathVariable Long id) {
        log.debug("REST request to delete with Periods BudgetItem : {}", id);
        budgetItemPeriodService.deleteFromBudgetItem(id);
        budgetItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/extend-budget-item-periods-and-next/{id}")
    public ResponseEntity<Void> extendBudgetItemPeriodAndNext(@PathVariable Long id) {
        log.debug("REST request to extend BudgetItemPeriod with BudgetItem: {}", id);
        Optional<BudgetItemDTO> budgetItemDTO = budgetItemService.findOne(id);
        if (budgetItemDTO.isPresent()) {
            budgetItemService.extendWithNext(budgetItemDTO.get());
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/last-budget-item-period/{id}")
    public ResponseEntity<BudgetItemPeriodDTO> getLastBudgetItemPeriod(@PathVariable Long id) {
        log.debug("REST request to get BudgetItem : {}", id);
        BudgetItemPeriodDTO budgetItemPeriodDTO = budgetItemService.findLastBudgetItemPeriod(id);
        return ResponseEntity.ok().body(budgetItemPeriodDTO);
    }

    @GetMapping("/budget-item-up-order/{id}")
    public ResponseEntity<BudgetItemDTO> upOrderBudgetItem(@PathVariable Long id) {
        log.debug("REST request to upOrderBudgetItem of: {}", id);
        Optional<BudgetItemDTO> budgetItemDTOOption = budgetItemService.findOne(id);
        if (budgetItemDTOOption.isPresent()) {
            BudgetItemDTO budgetItemDTO = budgetItemDTOOption.get();
            List<BudgetItemDTO> prevBudgetItemDTOList = budgetItemService.findPreviousOrderBudgetItem(budgetItemDTO);
            if (prevBudgetItemDTOList.size() != 0) {
                BudgetItemDTO prevBudgetItemDTO = prevBudgetItemDTOList.get(0);
                Integer prevOrder = prevBudgetItemDTO.getOrder();
                prevBudgetItemDTO.setOrder(budgetItemDTO.getOrder());
                budgetItemDTO.setOrder(prevOrder);
                budgetItemService.save(prevBudgetItemDTO);
                budgetItemService.save(budgetItemDTO);
            }
        }
        return ResponseUtil.wrapOrNotFound(budgetItemDTOOption);
    }

    @GetMapping("/budget-item-down-order/{id}")
    public ResponseEntity<BudgetItemDTO> downOrderBudgetItem(@PathVariable Long id) {
        log.debug("REST request to upOrderBudgetItem of: {}", id);
        Optional<BudgetItemDTO> budgetItemDTOOption = budgetItemService.findOne(id);
        if (budgetItemDTOOption.isPresent()) {
            BudgetItemDTO budgetItemDTO = budgetItemDTOOption.get();
            List<BudgetItemDTO> nextBudgetItemDTOList = budgetItemService.findNextOrderBudgetItem(budgetItemDTO);
            if (nextBudgetItemDTOList.size() != 0) {
                BudgetItemDTO nextBudgetItemDTO = nextBudgetItemDTOList.get(0);
                Integer nextOrder = nextBudgetItemDTO.getOrder();
                nextBudgetItemDTO.setOrder(budgetItemDTO.getOrder());
                budgetItemDTO.setOrder(nextOrder);
                budgetItemService.save(nextBudgetItemDTO);
                budgetItemService.save(budgetItemDTO);
            }
        }
        return ResponseUtil.wrapOrNotFound(budgetItemDTOOption);
    }

}
