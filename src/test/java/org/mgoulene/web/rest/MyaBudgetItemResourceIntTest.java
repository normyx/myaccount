package org.mgoulene.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.web.rest.TestUtil.createFormattingConversionService;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgoulene.MyaccountApp;
import org.mgoulene.domain.BudgetItem;
import org.mgoulene.domain.BudgetItemPeriod;
import org.mgoulene.domain.User;
import org.mgoulene.repository.BudgetItemPeriodRepository;
import org.mgoulene.repository.BudgetItemRepository;
import org.mgoulene.repository.UserRepository;
import org.mgoulene.service.BudgetItemPeriodQueryService;
import org.mgoulene.service.MyaBudgetItemPeriodService;
import org.mgoulene.service.MyaBudgetItemService;
import org.mgoulene.service.UserService;
import org.mgoulene.service.dto.BudgetItemDTO;
import org.mgoulene.service.dto.BudgetItemPeriodCriteria;
import org.mgoulene.service.dto.BudgetItemPeriodDTO;
import org.mgoulene.service.mapper.BudgetItemMapper;
import org.mgoulene.web.rest.errors.ExceptionTranslator;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Test class for the BudgetItemResource REST controller.
 *
 * @see BudgetItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyaccountApp.class)
public class MyaBudgetItemResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";

    private static final Integer DEFAULT_ORDER = 1;

    @Autowired
    private BudgetItemRepository budgetItemRepository;

    @Autowired
    private BudgetItemPeriodRepository budgetItemPeriodRepository;

    @Autowired
    private BudgetItemMapper budgetItemMapper;

    @Autowired
    private MyaBudgetItemService budgetItemService;

    @Autowired
    private MyaBudgetItemPeriodService budgetItemPeriodService;

    @Autowired
    private BudgetItemPeriodQueryService budgetItemPeriodQueryService;

    @Autowired
    private UserService userService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    private MockMvc restMyaBudgetItemMockMvc;

    private MockMvc restMyaBudgetItemPeriodMockMvc;

    private BudgetItem budgetItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MyaBudgetItemResource budgetItemResource = new MyaBudgetItemResource(budgetItemService, userService,
                budgetItemPeriodService);
        final MyaBudgetItemPeriodResource budgetItemPeriodResource = new MyaBudgetItemPeriodResource(budgetItemPeriodService,
                budgetItemPeriodQueryService);
        this.restMyaBudgetItemMockMvc = MockMvcBuilders.standaloneSetup(budgetItemResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .build();
        this.restMyaBudgetItemPeriodMockMvc = MockMvcBuilders.standaloneSetup(budgetItemPeriodResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static BudgetItem createEntity(EntityManager em) {
        BudgetItem budgetItem = new BudgetItem().name(DEFAULT_NAME).order(DEFAULT_ORDER);
        return budgetItem;
    }

    @Before
    public void initTest() {
        budgetItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createBudgetItemWithPeriod() throws Exception {
        // Clean before
        budgetItemRepository.deleteAll();
        budgetItemPeriodRepository.deleteAll();

        int databaseSizeBeforeCreate = budgetItemRepository.findAll().size();
        int budgetItemPeriodBeforeCreate = budgetItemPeriodRepository.findAll().size();
        // Create the BudgetItem
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/false/2018-03-01/-10/5")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());

        // Validate the BudgetItem in the database
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        List<BudgetItemPeriod> budgetItemPeriodList = budgetItemPeriodRepository.findAll();
        /*
         * Collections.sort(budgetItemPeriodList, new Comparator<BudgetItemPeriod>() {
         * public int compare(BudgetItemPeriod o1, BudgetItemPeriod o2) { return
         * o1.getMonth().compareTo(o2.getMonth()); } });
         */
        assertThat(budgetItemList).hasSize(databaseSizeBeforeCreate + 1);
        assertThat(budgetItemPeriodList).hasSize(budgetItemPeriodBeforeCreate + 10);
        int monthValue = 3;
        for (BudgetItemPeriod bip : budgetItemPeriodList) {
            assertThat(bip.getAmount()).isEqualTo(-10);
            // assertThat(bip.getDate().getDayOfMonth()).isEqualTo(5);
            assertThat(bip.isIsRecurrent()).isEqualTo(true);
            assertThat(bip.isIsSmoothed()).isEqualTo(false);

            assertThat(bip.getDate()).isEqualTo(LocalDate.of(2018, monthValue++, 5));
        }
        BudgetItem testBudgetItem = budgetItemList.get(budgetItemList.size() - 1);
        assertThat(testBudgetItem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBudgetItem.getOrder()).isEqualTo(DEFAULT_ORDER);
    }

    @Test
    @Transactional
    public void deleteBudgetItemWithPeriod() throws Exception {
        // Clean before
        budgetItemRepository.deleteAll();
        budgetItemPeriodRepository.deleteAll();

        // Create the BudgetItem
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/false/2018-03-01/-10/5")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());

        // Validate the BudgetItem in the database
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        List<BudgetItemPeriod> budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemList).hasSize(1);
        assertThat(budgetItemPeriodList).hasSize(10);
        restMyaBudgetItemMockMvc.perform(delete("/api/budget-items-with-periods/" + budgetItemList.get(0).getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
        budgetItemList = budgetItemRepository.findAll();
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemList).hasSize(0);
        assertThat(budgetItemPeriodList).hasSize(0);
    }

    @Test
    @Transactional
    public void manipulateBudgetItemPeriods() throws Exception {
        // Clean before
        budgetItemRepository.deleteAll();
        budgetItemPeriodRepository.deleteAll();

        int databaseSizeBeforeCreate = budgetItemRepository.findAll().size();
        int budgetItemPeriodBeforeCreate = budgetItemPeriodRepository.findAll().size();
        // Create the BudgetItem
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/false/2018-03-01/-10/5")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());

        // Validate the BudgetItem in the database
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        List<BudgetItemPeriod> budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemList).hasSize(1);
        assertThat(budgetItemPeriodList).hasSize(10);
        BudgetItem bi = budgetItemList.get(0);

        BudgetItemPeriodCriteria criteria = new BudgetItemPeriodCriteria();
        LongFilter biFilter = new LongFilter();
        biFilter.setEquals(budgetItemList.get(0).getId());
        criteria.setBudgetItemId(biFilter);
        LocalDateFilter monthFilter = new LocalDateFilter();
        monthFilter.setEquals(LocalDate.of(2018, 5, 1));
        criteria.setMonth(monthFilter);
        List<BudgetItemPeriodDTO> bipList = budgetItemPeriodQueryService.findByCriteria(criteria);
        assertThat(bipList).hasSize(1);
        BudgetItemPeriodDTO budgetItemPeriodDTO = bipList.get(0);

        budgetItemPeriodDTO.setAmount(-20.0f);

        budgetItemPeriodDTO.setDate(LocalDate.of(2018, 5, 6));

        restMyaBudgetItemPeriodMockMvc
                .perform(put("/api/budget-item-periods-and-next/6/true").contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());

        budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemPeriodList).hasSize(budgetItemPeriodBeforeCreate + 10);
        int monthValue = 3;

        for (BudgetItemPeriod bip : budgetItemPeriodList) {
            if (bip.getMonth().getMonthValue() < 5) {
                assertThat(bip.getAmount()).isEqualTo(-10);
                assertThat(bip.getDate()).isEqualTo(LocalDate.of(2018, monthValue++, 5));
            } else {
                assertThat(bip.getAmount()).isEqualTo(-20);
                assertThat(bip.getDate()).isEqualTo(LocalDate.of(2018, monthValue++, 6));
            }

            // assertThat(bip.getDate().getDayOfMonth()).isEqualTo(5);
            assertThat(bip.isIsRecurrent()).isEqualTo(true);
            assertThat(bip.isIsSmoothed()).isEqualTo(false);
        }
        assertThat(budgetItemPeriodList.get(4).getMonth()).isEqualTo(LocalDate.of(2018, 7, 1));
        restMyaBudgetItemPeriodMockMvc
                .perform(delete("/api/budget-item-periods-and-next/" + budgetItemPeriodList.get(4).getId())
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemPeriodList).hasSize(budgetItemPeriodBeforeCreate + 4);

        restMyaBudgetItemMockMvc.perform(post("/api/extend-budget-item-periods-and-next/" + bi.getId())
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isOk());
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemPeriodList).hasSize(budgetItemPeriodBeforeCreate + 10);
    }

    @Test
    @Transactional
    public void manipulateBudgetItemPeriodsWithLastDayOfMonth() throws Exception {
        // Create the BudgetItem
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/false/2018-01-01/-10/31")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());

        // Validate the BudgetItem in the database
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        List<BudgetItemPeriod> budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemList).hasSize(1);
        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 0; i < budgetItemPeriodList.size(); i++) {
            LocalDate date = budgetItemPeriodList.get(i).getDate();
            assertThat(date.getMonthValue()).isEqualTo(i + 1);
            assertThat(date.getDayOfMonth()).isEqualTo(date.lengthOfMonth());
        }

        BudgetItem bi = budgetItemList.get(0);

        BudgetItemPeriodCriteria criteria = new BudgetItemPeriodCriteria();
        LongFilter biFilter = new LongFilter();
        biFilter.setEquals(budgetItemList.get(0).getId());
        criteria.setBudgetItemId(biFilter);
        LocalDateFilter monthFilter = new LocalDateFilter();
        monthFilter.setEquals(LocalDate.of(2018, 1, 1));
        criteria.setMonth(monthFilter);
        List<BudgetItemPeriodDTO> bipList = budgetItemPeriodQueryService.findByCriteria(criteria);
        assertThat(bipList).hasSize(1);
        BudgetItemPeriodDTO budgetItemPeriodDTO = bipList.get(0);

        budgetItemPeriodDTO.setDate(LocalDate.of(2018, 1, 6));

        restMyaBudgetItemPeriodMockMvc
                .perform(put("/api/budget-item-periods-and-next/6/true").contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());

        budgetItemPeriodList = budgetItemPeriodRepository.findAll();
        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 0; i < budgetItemPeriodList.size(); i++) {
            LocalDate date = budgetItemPeriodList.get(i).getDate();
            assertThat(date.getMonthValue()).isEqualTo(i + 1);
            assertThat(date.getDayOfMonth()).isEqualTo(6);
        }

        criteria = new BudgetItemPeriodCriteria();
        biFilter = new LongFilter();
        biFilter.setEquals(budgetItemList.get(0).getId());
        criteria.setBudgetItemId(biFilter);
        monthFilter = new LocalDateFilter();
        monthFilter.setEquals(LocalDate.of(2018, 1, 1));
        criteria.setMonth(monthFilter);
        bipList = budgetItemPeriodQueryService.findByCriteria(criteria);
        assertThat(bipList).hasSize(1);
        budgetItemPeriodDTO = bipList.get(0);

        budgetItemPeriodDTO.setDate(LocalDate.of(2018, 1, 31));

        restMyaBudgetItemPeriodMockMvc
                .perform(put("/api/budget-item-periods-and-next/31/true").contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());

        budgetItemPeriodList = budgetItemPeriodRepository.findAll();
        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 0; i < budgetItemPeriodList.size(); i++) {
            LocalDate date = budgetItemPeriodList.get(i).getDate();
            assertThat(date.getMonthValue()).isEqualTo(i + 1);
            assertThat(date.getDayOfMonth()).isEqualTo(date.lengthOfMonth());
        }
        restMyaBudgetItemPeriodMockMvc
                .perform(delete("/api/budget-item-periods-and-next/" + budgetItemPeriodList.get(1).getId())
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();
        assertThat(budgetItemPeriodList).hasSize(1);

        restMyaBudgetItemMockMvc.perform(post("/api/extend-budget-item-periods-and-next/" + bi.getId())
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isOk());
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 1; i < budgetItemPeriodList.size(); i++) {
            LocalDate date = budgetItemPeriodList.get(i).getDate();
            assertThat(date.getMonthValue()).isEqualTo(i + 1);
            assertThat(date.getDayOfMonth()).isEqualTo(date.lengthOfMonth());
        }
    }

    @Test
    @Transactional
    public void manipulateBudgetItemPeriodsSmoothed() throws Exception {
        // Create the BudgetItem
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());

        // Validate the BudgetItem in the database
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        List<BudgetItemPeriod> budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemList).hasSize(1);
        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 0; i < budgetItemPeriodList.size(); i++) {

            assertThat(budgetItemPeriodList.get(i).isIsSmoothed()).isEqualTo(true);
            assertThat(budgetItemPeriodList.get(i).getAmount()).isEqualTo(-10);

        }

        BudgetItem bi = budgetItemList.get(0);

        BudgetItemPeriodCriteria criteria = new BudgetItemPeriodCriteria();
        LongFilter biFilter = new LongFilter();
        biFilter.setEquals(budgetItemList.get(0).getId());
        criteria.setBudgetItemId(biFilter);
        LocalDateFilter monthFilter = new LocalDateFilter();
        monthFilter.setEquals(LocalDate.of(2018, 1, 1));
        criteria.setMonth(monthFilter);
        List<BudgetItemPeriodDTO> bipList = budgetItemPeriodQueryService.findByCriteria(criteria);
        assertThat(bipList).hasSize(1);
        BudgetItemPeriodDTO budgetItemPeriodDTO = bipList.get(0);

        budgetItemPeriodDTO.setAmount(-20.0f);

        restMyaBudgetItemPeriodMockMvc
                .perform(put("/api/budget-item-periods-and-next/0/true").contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());

        budgetItemPeriodList = budgetItemPeriodRepository.findAll();
        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 0; i < budgetItemPeriodList.size(); i++) {

            assertThat(budgetItemPeriodList.get(i).isIsSmoothed()).isEqualTo(true);
            assertThat(budgetItemPeriodList.get(i).getAmount()).isEqualTo(-20);

        }

        restMyaBudgetItemPeriodMockMvc
                .perform(delete("/api/budget-item-periods-and-next/" + budgetItemPeriodList.get(1).getId())
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(budgetItemPeriodDTO)))
                .andExpect(status().isOk());
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();
        assertThat(budgetItemPeriodList).hasSize(1);

        restMyaBudgetItemMockMvc.perform(post("/api/extend-budget-item-periods-and-next/" + bi.getId())
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isOk());
        budgetItemPeriodList = budgetItemPeriodRepository.findAll();

        assertThat(budgetItemPeriodList).hasSize(12);
        for (int i = 0; i < budgetItemPeriodList.size(); i++) {
            LocalDate date = budgetItemPeriodList.get(i).getDate();
            assertThat(budgetItemPeriodList.get(i).isIsSmoothed()).isEqualTo(true);
            assertThat(budgetItemPeriodList.get(i).getAmount()).isEqualTo(-20);
        }
    }

    @Test
    @Transactional
    public void testBudgetItemPeriodOrderNextAndPrevious() throws Exception {
        // Create the BudgetItem
        budgetItem.name("budgetItem1");
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        budgetItem.name("budgetItem2");
        budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());

        // Validate the BudgetItem in the database
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        assertThat(budgetItemList).hasSize(2);
        for (BudgetItem bi : budgetItemList) {
            if (bi.getOrder() == 1) {
                assertThat(bi.getName()).isEqualTo("budgetItem1");
                List<BudgetItemDTO> items = budgetItemService.findNextOrderBudgetItem(budgetItemMapper.toDto(bi));
                assertThat(items).hasSize(1);
                assertThat(items.get(0).getName()).isEqualTo("budgetItem2");
            }
            if (bi.getOrder() == 2) {
                assertThat(bi.getName()).isEqualTo("budgetItem2");
                List<BudgetItemDTO> items = budgetItemService.findPreviousOrderBudgetItem(budgetItemMapper.toDto(bi));
                assertThat(items).hasSize(1);
                assertThat(items.get(0).getName()).isEqualTo("budgetItem1");
            }
        }

    }

    @Test
    @Transactional
    public void testNextOrder() throws Exception {
        // Create the BudgetItem
        budgetItem.order(1).name("budgetItem1");
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        Integer nextOrder = budgetItemService.findNewOrder(5L);
        assertThat(nextOrder).isEqualTo(2);
    }

    @Test
    @Transactional
    public void testBudgetItemGetEligibleItem() throws Exception {
        // Create the BudgetItem
        User user = userRepository.findOneByLogin("mgoulene").get();
        budgetItem.order(1).name("budgetItem1").account(user);
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build()
                .perform(get("/api/budget-eligible-items/2018-01-01/2018-12-01").with(user("mgoulene")))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$").isNotEmpty());
        MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build()
                .perform(get("/api/budget-eligible-items/2019-01-01/2019-12-01").with(user("mgoulene")))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void testLastBudgetItemPeriod() throws Exception {
        // Create the BudgetItem
        User user = userRepository.findOneByLogin("mgoulene").get();
        budgetItem.order(1).name("budgetItem1").account(user);
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/false/2018-01-01/-10/5")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        assertThat(budgetItemList).hasSize(1);
        restMyaBudgetItemMockMvc.perform(get("/api/last-budget-item-period/" + budgetItemList.get(0).getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.date").value("2018-12-05"));

    }

    @Test
    @Transactional
    public void testBudgetItemPeriodDown() throws Exception {
        // Create the BudgetItem
        budgetItem.order(1).name("budgetItem1");
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        budgetItem.order(2).name("budgetItem2");
        budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        assertThat(budgetItemList).hasSize(2);
        loop: for (BudgetItem bi : budgetItemList) {
            if (bi.getOrder() == 1) {
                restMyaBudgetItemMockMvc.perform(
                        get("/api/budget-item-down-order/" + bi.getId()).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                        .andExpect(status().isOk());
                List<BudgetItem> budgetItemList2 = budgetItemRepository.findAll();
                assertThat(budgetItemList2).hasSize(2);
                for (BudgetItem bi2 : budgetItemList2) {
                    if (bi2.getOrder() == 1) {
                        assertThat(bi2.getName()).isEqualTo("budgetItem2");
                    }
                    if (bi2.getOrder() == 2) {
                        assertThat(bi2.getName()).isEqualTo("budgetItem1");
                    }
                }
                break loop;
            }
        }
    }

    @Test
    @Transactional
    public void testBudgetItemPeriodUp() throws Exception {
        // Create the BudgetItem
        budgetItem.order(1).name("budgetItem1");
        BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        budgetItem.order(2).name("budgetItem2");
        budgetItemDTO = budgetItemMapper.toDto(budgetItem);
        budgetItemDTO.setAccountId(5L);
        restMyaBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/true/2018-01-01/-10/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                .andExpect(status().isCreated());
        List<BudgetItem> budgetItemList = budgetItemRepository.findAll();
        assertThat(budgetItemList).hasSize(2);
        loop: for (BudgetItem bi : budgetItemList) {
            if (bi.getOrder() == 2) {
                restMyaBudgetItemMockMvc.perform(
                        get("/api/budget-item-up-order/" + bi.getId()).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(budgetItemDTO)))
                        .andExpect(status().isOk());
                List<BudgetItem> budgetItemList2 = budgetItemRepository.findAll();
                assertThat(budgetItemList2).hasSize(2);
                for (BudgetItem bi2 : budgetItemList2) {
                    if (bi2.getOrder() == 1) {
                        assertThat(bi2.getName()).isEqualTo("budgetItem2");
                    }
                    if (bi2.getOrder() == 2) {
                        assertThat(bi2.getName()).isEqualTo("budgetItem1");
                    }
                }
                break loop;
            }
        }
    }

}
