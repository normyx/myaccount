package org.mgoulene.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.web.rest.TestUtil.createFormattingConversionService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.github.stefanbirkner.fakesftpserver.rule.FakeSftpServerRule;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgoulene.MyaccountApp;
import org.mgoulene.domain.BudgetItem;
import org.mgoulene.domain.BudgetItemPeriod;
import org.mgoulene.domain.Category;
import org.mgoulene.domain.SubCategory;
import org.mgoulene.domain.User;
import org.mgoulene.domain.enumeration.CategoryType;
import org.mgoulene.repository.BudgetItemPeriodRepository;
import org.mgoulene.repository.CategoryRepository;
import org.mgoulene.repository.SubCategoryRepository;
import org.mgoulene.repository.UserRepository;
import org.mgoulene.service.MyaBudgetItemPeriodService;
import org.mgoulene.service.MyaBudgetItemService;
import org.mgoulene.service.MyaOperationCSVImporterService;
import org.mgoulene.service.MyaOperationService;
import org.mgoulene.service.OperationQueryService;
import org.mgoulene.service.UserService;
import org.mgoulene.service.dto.BudgetItemDTO;
import org.mgoulene.service.mapper.BudgetItemMapper;
import org.mgoulene.web.rest.errors.ExceptionTranslator;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the OperationResource REST controller.
 *
 * @see OperationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyaccountApp.class)
public class MyaOperationResourceIntTest {

    @Autowired
    private MyaOperationService operationService;

    @Autowired
    private OperationQueryService operationQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Autowired
    private BudgetItemPeriodRepository budgetItemPeriodRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MyaOperationCSVImporterService operationCSVImporterService;

    @Autowired
    private MyaBudgetItemPeriodService budgetItemPeriodService;
    @Autowired
    private MyaBudgetItemService budgetItemService;

    @Autowired
    private UserService userService;

    @Autowired
    private BudgetItemMapper budgetItemMapper;

    @Rule
    public final FakeSftpServerRule sftpServer = new FakeSftpServerRule().addUser("user", "password").setPort(40015);

    private MockMvc restBudgetItemMockMvc;

    private MockMvc restOperationMockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MyaOperationResource operationResource = new MyaOperationResource(operationService, operationQueryService,
                budgetItemPeriodService, budgetItemService, operationCSVImporterService, userService);
        final MyaBudgetItemResource budgetItemResource = new MyaBudgetItemResource(budgetItemService, userService,
                budgetItemPeriodService);

        this.restOperationMockMvc = MockMvcBuilders.standaloneSetup(operationResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .build();
        this.restBudgetItemMockMvc = MockMvcBuilders.standaloneSetup(budgetItemResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .build();
    }

    @Test
    @Transactional
    public void testFindAllCloseOpertation() throws Exception {
        try {
            Category cat1;
            SubCategory subCat1;
            cat1 = new Category();
            cat1.setCategoryName("Cat1");
            cat1.setCategoryType(CategoryType.SPENDING);
            cat1 = categoryRepository.saveAndFlush(cat1);
            subCat1 = new SubCategory();
            subCat1.setSubCategoryName("sc1");
            subCat1.setCategory(cat1);
            subCat1 = subCategoryRepository.saveAndFlush(subCat1);
            // create Category and SubCategories

            User user = userRepository.findOneByLogin("mgoulene").get();
            // Import One Operation
            InputStream is = new ClassPathResource("./csv/opFromReportData.tsv").getInputStream();

            String operationString = IOUtils.toString(is, StandardCharsets.UTF_16);

            sftpServer.putFile("/home/in/mgoulene/operation.csv", operationString, StandardCharsets.UTF_16);

            restOperationMockMvc.perform(put("/api/import-operations-file")).andExpect(status().isOk());

            BudgetItem budgetItem = new BudgetItem().name("aaaaaaaa").category(cat1).order(1).account(user);

            BudgetItemDTO budgetItemDTO = budgetItemMapper.toDto(budgetItem);

            restBudgetItemMockMvc.perform(post("/api/budget-items-with-periods/false/2018-01-01/-10/5")
                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                    .content(TestUtil.convertObjectToJsonBytes(budgetItemDTO))).andExpect(status().isCreated());

            List<BudgetItemPeriod> bips = budgetItemPeriodRepository.findAll();

            assertThat(bips).hasSize(12);

            /*restOperationMockMvc.perform(get("/api/operations-close-to-budget/" + bips.get(0).getId()))
                    .andExpect(status().isOk()).andExpect(jsonPath("$[0].amount").value(-10)).andReturn();
            restOperationMockMvc.perform(get("/api/count-operations-close-to-budget/" + bips.get(0).getId()))
                    .andExpect(status().isOk()).andExpect(content().string("1")).andReturn();

            restOperationMockMvc.perform(get("/api/operations-close-to-budget/" + bips.get(5).getId()))
                    .andExpect(status().isOk()).andExpect(jsonPath("$[0].amount").value(-10)).andReturn();*/
        } catch (IOException e) {

            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }
}
